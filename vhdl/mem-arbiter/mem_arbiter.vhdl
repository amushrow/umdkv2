--
-- Copyright (C) 2012 Chris McClelland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.mem_ctrl_pkg.all;

entity mem_arbiter is
	generic (
		NO_MONITOR     : boolean
	);
	port (
		clk_in         : in  std_logic;
		clk2x_in       : in  std_logic;
		reset_in       : in  std_logic;

		-- Connetion to mem_pipe
		ppReady_out    : out std_logic;
		ppCmd_in       : in  MCCmdType;
		ppAddr_in      : in  std_logic_vector(22 downto 0);
		ppData_in      : in  std_logic_vector(15 downto 0);
		ppData_out     : out std_logic_vector(15 downto 0);
		ppRDV_out      : out std_logic;

		-- Connection to mem_ctrl
		mcAutoMode_out : out std_logic;
		mcReady_in     : in  std_logic;
		mcCmd_out      : out MCCmdType;
		mcAddr_out     : out std_logic_vector(22 downto 0);
		mcData_out     : out std_logic_vector(15 downto 0);
		mcUDQM_out     : out std_logic;
		mcLDQM_out     : out std_logic;
		mcData_in      : in  std_logic_vector(15 downto 0);
		mcRDV_in       : in  std_logic;
		mcBusy_in      : in  std_logic;

		-- Connection to MegaDrive
		mdDriveBus_out : out   std_logic;
		mdReset_in     : in    std_logic;
		mdDTACK_out    : out   std_logic;
		mdAddr_in      : in    std_logic_vector(22 downto 0);
		mdData_io      : inout std_logic_vector(15 downto 0);
		mdOE_in        : in    std_logic;
		mdAS_in        : in    std_logic;
		mdLDSW_in      : in    std_logic;
		mdUDSW_in      : in    std_logic;

		-- Trace pipe
		traceReset_in  : in  std_logic;
		traceEnable_in : in  std_logic;
		traceReady_in  : in  std_logic;
		traceData_out  : out std_logic_vector(55 downto 0);
		traceValid_out : out std_logic;

		-- MegaDrive registers
		regAddr_out     : out std_logic_vector(2 downto 0);
		regWrData_out   : out std_logic_vector(15 downto 0);
		regWrValid_out  : out std_logic;
		regRdData_in    : in  std_logic_vector(15 downto 0);
		regRdStrobe_out : out std_logic;
		regMapRam_in    : in  std_logic;
		reg32x_in       : in  std_logic
	);
end entity;

architecture rtl of mem_arbiter is
	type StateType is (
		S_RESET,  -- MD in reset, host has access to SDRAM
		S_IDLE,   -- wait for mdOE_sync to go low when A22='0', indicating a MD cart read

		-- Forced refresh loop on startup
		S_FORCE_REFRESH_EXEC,
		S_FORCE_REFRESH_NOP1,
		S_FORCE_REFRESH_NOP2,
		S_FORCE_REFRESH_NOP3,

		-- REGION HACK
		S_REGIONHACK_WAIT,
		S_REGIONHACK_NOP1,
		S_REGIONHACK_NOP2,
		S_REGIONHACK_NOP3,
		S_REGIONHACK_NOP4,
		S_REGIONHACK_REFRESH,
		S_REGIONHACK_FINISH,

		-- Owned read
		S_READ_OWNED_WAIT,
		S_READ_OWNED_NOP1,
		S_READ_OWNED_NOP2,
		S_READ_OWNED_NOP3,
		S_READ_OWNED_NOP4,
		S_READ_OWNED_REFRESH,
		S_READ_OWNED_FINISH,

		-- Foreign read
		S_READ_OTHER,

		-- Owned write
		S_WRITE_OWNED_NOP1,
		S_WRITE_OWNED_NOP2,
		S_WRITE_OWNED_NOP3,
		S_WRITE_OWNED_NOP4,
		S_WRITE_OWNED_EXEC,
		S_WRITE_OWNED_FINISH,

		-- Foreign write
		S_WRITE_OTHER_NOP1,
		S_WRITE_OTHER_NOP2,
		S_WRITE_OTHER_NOP3,
		S_WRITE_OTHER_NOP4,
		S_WRITE_OTHER_EXEC,
		S_WRITE_OTHER_FINISH,

		-- Register write
		S_WRITE_REG_NOP1,
		S_WRITE_REG_NOP2,
		S_WRITE_REG_NOP3,
		S_WRITE_REG_NOP4,
		S_WRITE_REG_EXEC,
		S_WRITE_REG_FINISH
	);
	type BankType is array (0 to 15) of std_logic_vector(4 downto 0);
	
	-- Registers
	signal state        : StateType := S_RESET;
	signal state_next   : StateType;
	signal dataReg      : std_logic_vector(15 downto 0) := (others => '0');
	signal dataReg_next : std_logic_vector(15 downto 0);
	signal addrReg      : std_logic_vector(22 downto 0) := (others => '0');
	signal addrReg_next : std_logic_vector(22 downto 0);
	signal sram_en      : std_logic := '0';
	signal sram_en_next : std_logic;
	signal addrStable   : std_logic := '0';
	constant STABLE     : unsigned(3 downto 0) := "0011";
	constant HB_MAX     : unsigned(10 downto 0) := (others => '1');
	constant DTACK_REQUEST_READ : unsigned(3 downto 0) := x"D";
	constant DTACK_REQUEST_WRITE : unsigned(3 downto 0) := x"9";
	
	signal hbCount      : unsigned(10 downto 0) := (others => '0');
	signal hbCount_next : unsigned(10 downto 0);
	signal tsCount      : unsigned(11 downto 0) := (others => '0');
	signal tsCount_next : unsigned(11 downto 0);
	signal ctrlFlags    : std_logic_vector(3 downto 0) := (others => '0'); --[3: Enable Saves, 2: Enable Region Hack, 1-0: Region]
	signal ctrlFlags_next  : std_logic_vector(3 downto 0);
	signal dtackCounter      : unsigned(3 downto 0) := (others => '0');
	signal dtackCounter_next : unsigned(3 downto 0);

	function BANK_INIT return BankType is
	begin
		return (
			"00000", "00001", "00010", "00011", "00100", "00101", "00110", "00111",
			"01000", "01001", "01010", "01011", "01100", "01101", "01110", "01111");
	end function;
	signal memBank      : BankType := BANK_INIT;
	signal memBank_next : BankType;
	signal bootInsn     : std_logic_vector(15 downto 0);

	-- Synchronise MegaDrive signals to sysClk
	signal mdOE_sync    : std_logic := '1';
	signal mdDSW_sync   : std_logic_vector(1 downto 0) := "11";
	signal mdAddr_sync  : std_logic_vector(22 downto 0) := (others => '0');
	signal mdData_sync  : std_logic_vector(15 downto 0) := (others => '0');
	constant TR_ORD     : std_logic_vector(3 downto 0) := "0011";
	constant TR_FRD     : std_logic_vector(3 downto 0) := "1011";
	constant TR_OW      : std_logic_vector(1 downto 0) := "00";
	constant TR_FW      : std_logic_vector(1 downto 0) := "10";
	constant TR_HB      : std_logic_vector(3 downto 0) := "0100";

begin
	-- Infer registers
	process(clk_in)
	begin
		if ( rising_edge(clk_in) ) then
			if ( reset_in = '1' ) then
				state <= S_RESET;
				dataReg <= (others => '0');
				addrReg <= (others => '0');
				hbCount <= (others => '0');
				tsCount <= (others => '0');
				memBank <= BANK_INIT;
				sram_en <= '0';
				ctrlFlags <= (others => '0');
				dtackCounter <= x"0";
			else
				state <= state_next;
				dataReg <= dataReg_next;
				addrReg <= addrReg_next;
				hbCount <= hbCount_next;
				tsCount <= tsCount_next;
				memBank <= memBank_next;
				sram_en <= sram_en_next;
				ctrlFlags <= ctrlFlags_next;
				dtackCounter <= dtackCounter_next;
			end if;
		end if;
	end process;

	process(clk2x_in)
	begin
		if ( rising_edge(clk2x_in) ) then
			if ( reset_in = '1' ) then
				mdAddr_sync <= (others => '0');
				mdOE_sync <= '1';
				mdDSW_sync <= "11";
				mdData_sync <= (others => '0');
			else
				if (mdAddr_in = mdAddr_sync) then
					addrStable <= (not mdOE_in) and (not mdOE_sync);
				else
					addrStable <= '0';
				end if;

				mdAddr_sync <= mdAddr_in;
				mdOE_sync <= mdOE_in;
				mdDSW_sync <= mdUDSW_in & mdLDSW_in;
				mdData_sync <= mdData_io;
			end if;
		end if;
	end process;

	-- #############################################################################################
	-- ##                           State machine to control the SDRAM                            ##
	-- #############################################################################################
	process(
			state, dataReg, addrReg, sram_en, ctrlFlags,
			mdOE_sync, mdDSW_sync, mdAddr_sync, mdData_sync, mdReset_in,
			mcReady_in, mcData_in, mcRDV_in,
			ppCmd_in, ppAddr_in, ppData_in,
			regMapRam_in, reg32x_in, bootInsn, memBank, regRdData_in,
			hbCount, tsCount, traceEnable_in, traceReset_in, traceReady_in, mdAddr_in, addrstable, mdOE_in, dtackCounter
		)
		-- Function to generate SDRAM physical address using MD address and memBank (SSF2) regs
		impure function transAddr(addr : std_logic_vector(22 downto 0)) return std_logic_vector is
		begin
			if (addr(22 downto 18) = "10110") then --0xB00000 - 0xB7FFFF
				return "11111" & addr(17 downto 0);
			else
				return memBank(to_integer(unsigned(addr(21 downto 18)))) & addr(17 downto 0);
			end if;
		end function;
	begin
		-- Local register defaults
		state_next <= state;
		dataReg_next <= dataReg;
		addrReg_next <= addrReg;
		memBank_next <= memBank;
		sram_en_next <= sram_en;
		ctrlFlags_next <= ctrlFlags;

		-- Memory controller defaults
		mcAutoMode_out <= '0';  -- don't auto-refresh by default.
		mcCmd_out <= MC_NOP;
		mcAddr_out <= (others => 'X');
		mcData_out <= (others => 'X');
		mcUDQM_out <= '0';
		mcLDQM_out <= '0';

		-- Pipe defaults
		ppData_out <= (others => 'X');
		ppReady_out <= '0';
		ppRDV_out <= '0';

		-- Trace defaults
		traceData_out <= (others => 'X');
		traceValid_out <= '0';

		-- MegaDrive registers
		regAddr_out <= (others => 'X');
		regWrData_out <= (others => 'X');
		regWrValid_out <= '0';
		regRdStrobe_out <= '0';

		-- MegaDrive data bus
		mdData_io <= (others => 'Z');
		mdDriveBus_out <= '0';

		-- Maybe send heartbeat message
		if ( traceReset_in = '1' ) then
			tsCount_next <= (others => '0');
			hbCount_next <= (others => '0');
		else
			tsCount_next <= tsCount + 1;
			hbCount_next <= hbCount + 1;
		end if;
		if ( hbCount = HB_MAX ) then
			traceData_out <= TR_HB & std_logic_vector(tsCount) & x"000000" & x"0000";
			traceValid_out <= traceEnable_in;
		end if;

		if (dtackCounter > 0) then
			dtackCounter_next <= dtackCounter - 1;
		else
			dtackCounter_next <= dtackCounter;
		end if;

		case state is
			-- -------------------------------------------------------------------------------------
			-- Whilst the MD is in reset, the SDRAM does auto-refresh, and the host has complete
			-- control over it.
			--
			when S_RESET =>
				-- Enable auto-refresh
				mcAutoMode_out <= '1';

				-- Drive mem-ctrl inputs with mem-pipe outputs
				mcCmd_out <= ppCmd_in;
				mcAddr_out <= ppAddr_in;
				mcData_out <= ppData_in;

				-- Drive mem-pipe inputs with mem-ctrl outputs
				ppData_out <= mcData_in;
				ppReady_out <= mcReady_in;  
				ppRDV_out <= mcRDV_in;

				-- Proceed when host or the soft-reset sequence releases MD from reset
				if ( mdReset_in = '0' and mcReady_in = '1' ) then
					state_next <= S_FORCE_REFRESH_EXEC;
				end if;

			-- -------------------------------------------------------------------------------------
			-- There's a delay of ~100ms between deasserting reset and the first instruction-fetch,
			-- which can be used profitably by forcing a series of SDRAM refresh cycles.
			--
			when S_FORCE_REFRESH_EXEC =>
				mcCmd_out <= MC_REF;  -- issue refresh cycle
				state_next <= S_FORCE_REFRESH_NOP1;

			when S_FORCE_REFRESH_NOP1 =>
				state_next <= S_FORCE_REFRESH_NOP2;

			when S_FORCE_REFRESH_NOP2 =>
				state_next <= S_FORCE_REFRESH_NOP3;

			when S_FORCE_REFRESH_NOP3 =>
				if ( mdOE_sync = '1' ) then
					state_next <= S_FORCE_REFRESH_EXEC;  -- loop back for another refresh
				else
					state_next <= S_IDLE;                -- 68000 has started fetching
				end if;

			-- When we get a request to the version register, also drive the bus to force a different region
			-- (it's not very friendly, but it works)
			when S_REGIONHACK_WAIT =>
				state_next <= S_REGIONHACK_NOP1;

			when S_REGIONHACK_NOP1 =>
				state_next <= S_REGIONHACK_NOP2;

			when S_REGIONHACK_NOP2 =>
				dataReg_next <= ctrlFlags(1 downto 0) & mdData_sync(5 downto 0) & ctrlFlags(1 downto 0) & mdData_sync(5 downto 0);
				mdData_io <= ctrlFlags(1 downto 0) & mdData_sync(5 downto 0) & ctrlFlags(1 downto 0) & mdData_sync(5 downto 0);
				mdDriveBus_out <= '1';
				state_next <= S_REGIONHACK_NOP3;

			when S_REGIONHACK_NOP3 =>
				mdData_io <= dataReg;
				mdDriveBus_out <= '1';
				state_next <= S_REGIONHACK_NOP4;

			when S_REGIONHACK_NOP4 =>
				mdData_io <= dataReg;
				mdDriveBus_out <= '1';
				state_next <= S_REGIONHACK_REFRESH;
				
			-- Start a refresh cycle, then wait for it to complete.
			--
			when S_REGIONHACK_REFRESH =>
				mdData_io <= dataReg;
				mdDriveBus_out <= '1';
				state_next <= S_REGIONHACK_FINISH;
				mcCmd_out <= MC_REF;

			when S_REGIONHACK_FINISH =>
				mdData_io <= dataReg;
				mdDriveBus_out <= '1';
				if ( mcReady_in = '1' and mdOE_sync = '1' ) then
					state_next <= S_IDLE;
				end if;

			-- -------------------------------------------------------------------------------------
			-- Wait until the in-progress owned read completes, then register the result, send to
			-- the trace FIFO and proceed.
			--
			when S_READ_OWNED_WAIT =>
				if ( mcRDV_in = '1' ) then
					mdDriveBus_out <= '1';
					if (reg32x_in = '1') then
						state_next <= S_READ_OWNED_REFRESH;
					else
						state_next <= S_READ_OWNED_NOP1;
					end if;

					if ( regMapRam_in = '1' ) then
						mdData_io <= mcData_in;
						dataReg_next <= mcData_in;
					else
						mdData_io <= bootInsn;
						dataReg_next <= bootInsn;
					end if;
				end if;

			-- Give the host enough time for one I/O cycle, if it wants it.
			--
			when S_READ_OWNED_NOP1 =>
				mdData_io <= dataReg;
				mdDriveBus_out <= '1';
				ppReady_out <= mcReady_in;
				mcCmd_out <= ppCmd_in;
				mcAddr_out <= ppAddr_in;
				mcData_out <= ppData_in;
				state_next <= S_READ_OWNED_NOP2;

			when S_READ_OWNED_NOP2 =>
				mdData_io <= dataReg;
				mdDriveBus_out <= '1';
				state_next <= S_READ_OWNED_NOP3;

			when S_READ_OWNED_NOP3 =>
				mdData_io <= dataReg;
				mdDriveBus_out <= '1';
				state_next <= S_READ_OWNED_NOP4;

			when S_READ_OWNED_NOP4 =>
				mdData_io <= dataReg;
				mdDriveBus_out <= '1';
				ppData_out <= mcData_in;
				ppRDV_out <= mcRDV_in;
				state_next <= S_READ_OWNED_REFRESH;

			-- Start a refresh cycle, then wait for it to complete.
			--
			when S_READ_OWNED_REFRESH =>
				mdDriveBus_out <= '1';
				
				traceData_out <= TR_ORD & std_logic_vector(tsCount) & addrReg & '0' & dataReg;
				traceValid_out <= traceEnable_in;

				-- Only do a refresh now if this read hasn't finished yet
				if (mdOE_sync = '1') then
					state_next <= S_IDLE;
				else
					state_next <= S_READ_OWNED_FINISH;
					mcCmd_out <= MC_REF;
				end if;

			when S_READ_OWNED_FINISH =>
				mdData_io <= dataReg;
				mdDriveBus_out <= '1';
				
				if ( mcReady_in = '1' and mdOE_sync = '1') then
					state_next <= S_IDLE;
					dtackCounter_next <= x"0";
				end if;

			-- -------------------------------------------------------------------------------------
			-- Wait for the in-progress foreign read to complete, then send to the trace FIFO and go
			-- back to S_IDLE.
			--
			when S_READ_OTHER =>
				if ( mdOE_sync = '1' ) then
					state_next <= S_IDLE;
					traceData_out <= TR_FRD & std_logic_vector(tsCount) & addrReg & '0' & mdData_sync;
					traceValid_out <= traceEnable_in;
					hbCount_next <= (others => '0');  -- reset heartbeat
				end if;

			-- -------------------------------------------------------------------------------------
			-- An owned write has been requested, but things are not yet stable so give the host
			-- enough time for one I/O cycle, if it wants it - this will provide enough of a delay
			-- for the write masks and data to stabilise.
			--
			when S_WRITE_OWNED_NOP1 =>
				if (sram_en = '1' and addrReg(22 downto 15) = x"20") then
					--Save it being written to, set a timer for the menu program
					--to write teh save to the SD card
					mcCmd_out <= MC_WR;
					mcAddr_out <= "11111001000011010000000"; --0x7C8680 (0x410D00 on the MD), Fixed address which signals to the menu that the save has been updated
					mcData_out <= x"000F";
				else
					ppReady_out <= mcReady_in;
					mcCmd_out <= ppCmd_in;
					mcAddr_out <= ppAddr_in;
					mcData_out <= ppData_in;
				end if;

				state_next <= S_WRITE_OWNED_NOP2;

			when S_WRITE_OWNED_NOP2 =>
				state_next <= S_WRITE_OWNED_NOP3;

			when S_WRITE_OWNED_NOP3 =>
				state_next <= S_WRITE_OWNED_NOP4;

			when S_WRITE_OWNED_NOP4 =>
				ppData_out <= mcData_in;
				ppRDV_out <= mcRDV_in;
				state_next <= S_WRITE_OWNED_EXEC;
				
			-- Now execute the owned write.
			--
			when S_WRITE_OWNED_EXEC =>
				
				state_next <= S_WRITE_OWNED_FINISH;
				traceData_out <= TR_OW & "00" & std_logic_vector(tsCount) & addrReg & '0' & mdData_sync;
				traceValid_out <= traceEnable_in;

				if (sram_en = '1' and addrReg(22 downto 15) = x"20") then
					--Last 64Kb of SDRAM
					mcCmd_out <= MC_WR;
					mcAddr_out <= "11111111" & addrReg(14 downto 0);
					mcData_out <= mdData_sync;
					mcUDQM_out <= mdDSW_sync(1);
					mcLDQM_out <= mdDSW_sync(0);
				elsif ( addrReg(22) = '0' or addrReg(22 downto 18) = "10110") then
					mcCmd_out <= MC_WR;
					mcAddr_out <= transAddr(addrReg);
					mcData_out <= mdData_sync;
					mcUDQM_out <= mdDSW_sync(1);
					mcLDQM_out <= mdDSW_sync(0);
				end if;

			when S_WRITE_OWNED_FINISH =>
				if ( (mdDSW_sync = "11" or mdOE_sync = '0') and mcReady_in = '1' ) then
					state_next <= S_IDLE;
					dtackCounter_next <= x"0";
				end if;

			-- -------------------------------------------------------------------------------------
			-- A foreign write has been requested, but things are not yet stable so give the host
			-- enough time for one I/O cycle, if it wants it - this will provide enough of a delay
			-- for the write masks and data to stabilise.
			--
			when S_WRITE_OTHER_NOP1 =>
				ppReady_out <= mcReady_in;
				mcCmd_out <= ppCmd_in;
				mcAddr_out <= ppAddr_in;
				mcData_out <= ppData_in;
				state_next <= S_WRITE_OTHER_NOP2;

			when S_WRITE_OTHER_NOP2 =>
				state_next <= S_WRITE_OTHER_NOP3;

			when S_WRITE_OTHER_NOP3 =>
				state_next <= S_WRITE_OTHER_NOP4;

			when S_WRITE_OTHER_NOP4 =>
				ppData_out <= mcData_in;
				ppRDV_out <= mcRDV_in;
				state_next <= S_WRITE_OTHER_EXEC;

			-- Now execute the foreign write - it'll be handled by someone else so just copy it over
			-- to the trace FIFO.
			--
			when S_WRITE_OTHER_EXEC =>
				state_next <= S_WRITE_OTHER_FINISH;
				traceData_out <= TR_FW & mdDSW_sync & std_logic_vector(tsCount) & addrReg & '0' & mdData_sync;
				traceValid_out <= traceEnable_in;
				hbCount_next <= (others => '0');  -- reset heartbeat

			when S_WRITE_OTHER_FINISH =>
				if ( (mdDSW_sync = "11" or mdOE_sync = '0') and mcReady_in = '1' ) then
					state_next <= S_IDLE;
				end if;

			-- -------------------------------------------------------------------------------------
			-- A register write has been requested, but things are not yet stable so give the host
			-- enough time for one I/O cycle, if it wants it - this will provide enough of a delay
			-- for the write masks and data to stabilise.
			--
			when S_WRITE_REG_NOP1 =>
				if ( addrReg(6 downto 0) = "1111000" and ctrlFlags(3) = '1') then
					-- Map 'SRAM'
					-- Patch in our own VINT to copy saves to the SD card (first word)
					mcCmd_out <= MC_WR;
					mcAddr_out <= "00000000000000000111100"; --0x78
					mcData_out <= x"00B2";
				else
					ppReady_out <= mcReady_in;
					mcCmd_out <= ppCmd_in;
					mcAddr_out <= ppAddr_in;
					mcData_out <= ppData_in;
				end if;

				state_next <= S_WRITE_REG_NOP2;

			when S_WRITE_REG_NOP2 =>
				state_next <= S_WRITE_REG_NOP3;

			when S_WRITE_REG_NOP3 =>
				state_next <= S_WRITE_REG_NOP4;

			when S_WRITE_REG_NOP4 =>
				ppData_out <= mcData_in;
				ppRDV_out <= mcRDV_in;
				state_next <= S_WRITE_REG_EXEC;

			-- Now execute the register write.
			--
			when S_WRITE_REG_EXEC =>
				state_next <= S_WRITE_REG_FINISH;
				traceData_out <= TR_OW & mdDSW_sync & std_logic_vector(tsCount) & addrReg & '0' & mdData_sync;
				traceValid_out <= traceEnable_in;
				hbCount_next <= (others => '0');  -- reset heartbeat

				if ( addrReg(6 downto 0) = "1110000" ) then
					-- Region override
					ctrlFlags_next <= mdData_sync(3 downto 0);

				elsif ( addrReg(6 downto 0) = "1111000") then
					-- Map 'SRAM'
					if (mdData_sync(7 downto 0) = "00000000") then
						sram_en_next <= '0';
					else
						sram_en_next <= '1';
					end if;
					
					if (ctrlFlags(3) = '1') then
						-- Patch in our own VINT to copy saves to the SD card (second word)
						mcCmd_out <= MC_WR;
						mcAddr_out <= "00000000000000000111101"; --0x7A
						mcData_out <= x"0200";
					end if;

				elsif ( addrReg(6 downto 3) = "1111" ) then
					memBank_next(to_integer(unsigned(mdData_sync(6) & addrReg(2 downto 0)))) <= mdData_sync(4 downto 0);
				elsif ( addrReg(6 downto 3) = "0000" ) then
					regAddr_out <= addrReg(2 downto 0);
					regWrData_out <= mdData_sync;
					regWrValid_out <= '1';
				end if;

			when S_WRITE_REG_FINISH =>
				if ( (mdDSW_sync = "11" or mdOE_sync = '0') and mcReady_in = '1' ) then
					state_next <= S_IDLE;
				end if;

			-- -------------------------------------------------------------------------------------
			-- S_IDLE & others.
			--
			when others =>
				-- See if the host wants MD back in reset
				if ( mdReset_in = '1' ) then
					-- MD back in reset, so give host full control again
					state_next <= S_RESET;
				end if;

				if ( mdOE_sync = '0') then
					if (addrStable = '1') then
						-- MD is reading
						addrReg_next <= mdAddr_in;
					
						if (mdAddr_in & '1' = x"A10001" and ctrlFlags(2) = '1') then
							--0xA10001, version register
							state_next <= S_REGIONHACK_WAIT;
						elsif ( mdAddr_in(22 downto 3) = x"A130E" ) then
							--- MARS check address
							state_next <= S_READ_OTHER;
						elsif ( mdAddr_in(22 downto 7) = x"A130" ) then
							-- MD is reading the 0xA130xx range
							if (reg32x_in = '1') then
								state_next <= S_READ_OWNED_REFRESH;
							else
								state_next <= S_READ_OWNED_NOP1;
							end if;

							regAddr_out <= mdAddr_in(2 downto 0);
							dataReg_next <= regRdData_in;
							regRdStrobe_out <= '1';
							traceData_out <= TR_ORD & std_logic_vector(tsCount) & mdAddr_in & '0' & regRdData_in;
							traceValid_out <= traceEnable_in;
						
						elsif (mdAddr_in(22 downto 18) = "10110") then --0xB00000 - 0xB7FFFF
							-- MD is reading the 0xB000xx range
							state_next <= S_READ_OWNED_WAIT;
							mcCmd_out <= MC_RD;
							mcAddr_out <= transAddr(mdAddr_in);
							dtackCounter_next <= DTACK_REQUEST_READ;

						--Owned read for ranges 0x000000 - 0x3FFFFF & 0x800000 - 0x8FFFFF
						elsif (mdAddr_in(22 downto 21) = "00" or (reg32x_in = '1' and mdAddr_in(22 downto 19) = "1000") or (reg32x_in = '0' and mdAddr_in(22) = '0')) then 
							-- MD is doing an owned read (i.e in our address ranges)
							state_next <= S_READ_OWNED_WAIT;
							mcCmd_out <= MC_RD;
						
							if (sram_en = '1' and mdAddr_in(22 downto 15) = x"20") then
								--Get last 64Kb of SDRAM
								mcAddr_out <= "11111111" & mdAddr_in(14 downto 0);
							else
								mcAddr_out <= transAddr("00" & mdAddr_in(20 downto 0));
							end if;
						else
							-- MD is doing a foreign read (i.e not in our address ranges)
							state_next <= S_READ_OTHER;
						end if;
					end if;
				elsif ( mdDSW_sync /= "11" ) then
					-- MD is writing
					addrReg_next <= mdAddr_sync;
					if ( mdAddr_sync(22 downto 7) = x"A130" ) then
						-- MD is writing 0xA130xx range
						state_next <= S_WRITE_REG_NOP1;
					elsif ( mdAddr_sync(22 downto 18) = "10110") then
						-- MD is writing 0xB00000 - 0xB7FFFF range
						state_next <= S_WRITE_OWNED_NOP1;
						dtackCounter_next <= DTACK_REQUEST_WRITE;
					elsif (reg32x_in = '0' and mdAddr_sync(22) = '0' ) then
						-- MD is doing an owned write (i.e in our address ranges)
						state_next <= S_WRITE_OWNED_NOP1;
					elsif (reg32x_in = '0') then
						-- MD is doing a foreign write (i.e not in our address ranges)
						state_next <= S_WRITE_OTHER_NOP1;
					else
						-- Don't have time to execute host commands during a foreign write on the 32x
						--traceData_out <= TR_FW & mdDSW_sync & std_logic_vector(tsCount) & mdAddr_sync & '0' & mdData_sync;
						--traceValid_out <= traceEnable_in;
						--hbCount_next <= (others => '0');  -- reset heartbeat
						state_next <= S_WRITE_OTHER_FINISH;
					end if;
				end if;

		end case;
	end process;

	with dtackCounter select mdDTACK_out <=
		'1' when x"1",
		'1' when x"2",
		'1' when x"3",
		'1' when x"4",
		'1' when x"5",
		'1' when x"6",
		'1' when x"7",
		'1' when x"8",
		'1' when x"9",
		'1' when x"A",
		'0' when others;
	
	-- Boot ROM - just load the bootblock from flash into onboard RAM and start it running
	with addrReg(8 downto 0) select bootInsn <=
		x"0000" when "000000000", -- initial SSP
		x"0000" when "000000001",
		x"0000" when "000000010", -- initial PC
		x"0008" when "000000011",
		x"41F9" when "000000100", -- lea 0xA13000, a0
		x"00A1" when "000000101",
		x"3000" when "000000110",
		x"43F9" when "000000111", -- lea 0xFF0000, a1
		x"00FF" when "000001000",
		x"0000" when "000001001",
		x"317C" when "000001010", -- move.w #(TURBO|FLASH), 4(a0)
		x"0005" when "000001011",
		x"0004" when "000001100",
		x"30BC" when "000001101", -- move.w #0x0306, (a0)
		x"0306" when "000001110",
		x"30BC" when "000001111", -- move.w #0x0000, (a0)
		x"0000" when "000010000",
		x"30BC" when "000010001", -- move.w #0xFFFF, (a0)
		x"FFFF" when "000010010",
		x"707F" when "000010011", -- moveq #127, d0 ; copy 128 words = 256 bytes
		x"32D0" when "000010100", -- move.w (a0), (a1)+
		x"51C8" when "000010101", -- dbra d0, *-4
		x"FFFC" when "000010110",
		x"4EF9" when "000010111", -- jmp 0xFF0000
		x"00FF" when "000011000",
		x"0000" when "000011001",

		x"5345" when "010000000",
		x"4741" when "010000001",
		(others => 'X') when others;

	-- A verifiable test bootblock can be installed like this:
	--
	-- $ printf '\x31\x7C\x00\x00\x00\x04\x33\xFC\x60\xFE\x00\x40\x00\x00\x4E\xF9\x00\x40\x00\x00' > bra.bin
	-- $ $HOME/makestuff/apps/flcli/lin.x64/rel/flcli -v 1d50:602b:0002 -p J:A7A0A3A1:spi-talk.xsvf
	-- $ $HOME/makestuff/apps/gordon/lin.x64/rel/gordon -v 1d50:602b:0002 -t indirect:1 -w bra.bin:0x60000
	-- $ $HOME/makestuff/apps/gordon/lin.x64/rel/gordon -v 1d50:602b:0002 -t indirect:1 -r foo.bin:0x60000:256
	-- $ dis68 foo.bin 0 3
	-- 0x000000  move.w #0, 4(a0)
	-- 0x000006  move.w #0x60FE, 0x400000 ; opcode for bra.s -2
	-- 0x00000E  jmp 0x400000
	-- 
	-- It consists of the opcodes to deselect the flash (and thereby map in the SDRAM), then write
	-- the opcode for bra.s -2 to 0x400000 and jump to it. Reads from onboard RAM don't show on the
	-- trace, but you can see the infinite bra.s -2 loop executing OK.
	
end architecture;