#ifndef FILES_H
#define FILES_H

#include "fat32.h"
#include "sdcard.h"
#include "qsort.h"

#define MDROM 0x01
#define SMSROM 0x02
#define XROM 0x04
#define SAVE 0x08

extern s16 dirListLen;
extern FileInfo **dirList, **dirPtr;
void initFiles(u16 *freeMem);
void storeFile(const FileInfo* fileInfo);
short myFileComp(CVPtr x, CVPtr y);

#endif
