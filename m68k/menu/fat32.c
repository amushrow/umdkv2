#include "fat32.h"
#include "genesis.h"

//FileInfo struct with enough space for the max filename
typedef struct struct_FileInfoFull
{
	FileInfo f;
	char FilenameBuffer[20 * 13 + 2];
} FileInfoFull;

int fatListDirectoryInternal(FileSystem* fs, TFFile* dir, FileInfoCallback callback)
{
	FileInfoFull Info;
	u8 offset;
	u8 last;
	u16 fnLen = 0;
	char* FilenameBuffer = &Info.f.Filename[0];

	FatFileEntry CurrentEntry;
	tf_fread(fs, (u8*)&CurrentEntry, sizeof(FatFileEntry), dir);

	for (; ; )
	{
		if (CurrentEntry.msdos.filename[0] == '\x00')
		{
			break; //No more entries
		}
		else if (CurrentEntry.msdos.attributes == 0x0F) //Long filename
		{
			SwizzleFFLFN(&CurrentEntry.lfn);

			offset = CurrentEntry.lfn.sequence_number;
			last = offset & 0x40;
			offset &= 0x1F;
			offset--;
			offset *= 13;
			FilenameBuffer[offset + 0] = CurrentEntry.lfn.name1[0] & 0xFF;
			FilenameBuffer[offset + 1] = CurrentEntry.lfn.name1[1] & 0xFF;
			FilenameBuffer[offset + 2] = CurrentEntry.lfn.name1[2] & 0xFF;
			FilenameBuffer[offset + 3] = CurrentEntry.lfn.name1[3] & 0xFF;
			FilenameBuffer[offset + 4] = CurrentEntry.lfn.name1[4] & 0xFF;
			FilenameBuffer[offset + 5] = CurrentEntry.lfn.name2[0] & 0xFF;
			FilenameBuffer[offset + 6] = CurrentEntry.lfn.name2[1] & 0xFF;
			FilenameBuffer[offset + 7] = CurrentEntry.lfn.name2[2] & 0xFF;
			FilenameBuffer[offset + 8] = CurrentEntry.lfn.name2[3] & 0xFF;
			FilenameBuffer[offset + 9] = CurrentEntry.lfn.name2[4] & 0xFF;
			FilenameBuffer[offset + 10] = CurrentEntry.lfn.name2[5] & 0xFF;
			FilenameBuffer[offset + 11] = CurrentEntry.lfn.name3[0] & 0xFF;
			FilenameBuffer[offset + 12] = CurrentEntry.lfn.name3[1] & 0xFF;

			if (last)
			{
				FilenameBuffer[offset + 13] = '\0';
				FilenameBuffer[offset + 14] = '\0';
				offset += 12;
				while (FilenameBuffer[offset] == 0 || FilenameBuffer[offset] == -1) {
					FilenameBuffer[offset] = 0;
					offset--;
				}
				offset++;
				fnLen = offset;
			}
		}
		else
		{
			if (CurrentEntry.msdos.filename[0] != (char)0xE5 && CurrentEntry.msdos.attributes != TF_ATTR_VOLUME_LABEL)
			{
				SwizzleFF83(&CurrentEntry.msdos);
				if (fnLen == 0)
				{
					//No long name, use short name
					for (int k = 0; k < 8 && CurrentEntry.msdos.filename[k] != ' '; ++k)
						FilenameBuffer[fnLen++] = CurrentEntry.msdos.filename[k];

					//Extension
					if (CurrentEntry.msdos.filename[8] != ' ')
					{
						FilenameBuffer[fnLen++] = '.';
						for (int ext = 0; ext < 3; ++ext)
							FilenameBuffer[fnLen++] = CurrentEntry.msdos.filename[8 + ext];
					}

					FilenameBuffer[fnLen] = 0;
				}

				Info.f.ParentStartCluster = dir->startCluster;
				Info.f.StartCluster = ((u32)(CurrentEntry.msdos.eaIndex & 0xffff) << 16) | (CurrentEntry.msdos.firstCluster & 0xffff);
				if (Info.f.StartCluster == 0)
					Info.f.StartCluster = fs->tf_info.rootCluster;

				Info.f.Attributes = CurrentEntry.msdos.attributes;
				if (CurrentEntry.msdos.attributes & TF_ATTR_DIRECTORY)
					Info.f.Size = 0XFFFFFFFF;
				else
					Info.f.Size = CurrentEntry.msdos.fileSize;

				callback(&Info.f);
			}

			fnLen = 0;
		}

		//Advance to next entry
		if (tf_fread(fs, (u8*)&CurrentEntry, sizeof(FatFileEntry), dir) != sizeof(FatFileEntry))
			break;
	};

	return 0;
}

void fatInternal_UnsafeDeleteFI(FileSystem* fs, FileInfo* info)
{
	//Just delete the thing
	FileInfo Parent = *info;
	Parent.StartCluster = Parent.ParentStartCluster;
	TFFile* fp = fatOpenFileFI(fs, &Parent, "r+");

	// Find entry in parent directory
	FatFileEntry CurrentEntry;
	tf_fread(fs, (u8*)&CurrentEntry, sizeof(FatFileEntry), fp);
	u8 FoundEntry = 0;
	u8 HaveLFN = 0;
	u32 LFNStartPos = 0;

	while (1)
	{
		if (CurrentEntry.msdos.filename[0] == '\x00')
		{
			break; //No more entries
		}
		else if (CurrentEntry.msdos.attributes == 0x0F) //Long filename
		{
			if (HaveLFN == 0)
			{
				HaveLFN = 1;
				LFNStartPos = fp->pos - sizeof(FatFileEntry);
			}
		}
		else
		{
			if (CurrentEntry.msdos.filename[0] != (char)0xE5)
			{
				// Found entry, store it and stop looking
				SwizzleFF83(&CurrentEntry.msdos);
				u32 StartCluster = ((u32)(CurrentEntry.msdos.eaIndex & 0xffff) << 16) | (CurrentEntry.msdos.firstCluster & 0xffff);
				if (StartCluster == info->StartCluster)
				{
					FoundEntry = 1;
					break;
				}
			}

			HaveLFN = 0;
		}

		//Advance to next entry
		tf_fread(fs, (u8*)&CurrentEntry, sizeof(FatFileEntry), fp);
	}

	//Remove the entries from the list
	if (FoundEntry)
	{
		u32 EntrySize = sizeof(FatFileEntry);
		if (HaveLFN)
			EntrySize = fp->pos - LFNStartPos;

		while (1)
		{
			//Shift all entries back
			tf_fread(fs, (u8*)&CurrentEntry, sizeof(FatFileEntry), fp);
			tf_fseek(fs, fp, -(EntrySize + sizeof(FatFileEntry)), fp->pos);
			tf_fwrite(fs, (u8*)&CurrentEntry, sizeof(FatFileEntry), fp);
			tf_fseek(fs, fp, fp->pos, EntrySize);

			if (CurrentEntry.msdos.filename[0] == 0)
				break;
		}
		fp->size -= EntrySize;
		fp->flags |= TF_FLAG_SIZECHANGED;
	}
	tf_fclose(fs, fp);
	tf_free_clusterchain(fs, info->StartCluster); // Free the data associated with the file
}

u32 fatGetMemoryFootprint()
{
	return tf_get_memory_footprint();
}

int fatOpenFileSystem(FileSystem* fs)
{
	return tf_init(fs);
}

// List the contents of a directory
void fatListDirectory(FileSystem* fs, const char* directoryName, FileInfoCallback callback)
{
	TFFile* file = tf_fopen(fs, directoryName, "r");
	fatListDirectoryInternal(fs, file, callback);
	tf_fclose(fs, file);
}

// List the contents of a directory
void fatListDirectoryFI(FileSystem* fs, const FileInfo* directoryInfo, FileInfoCallback callback)
{
	TFFile* file = fatOpenFileFI(fs, directoryInfo, "r");
	fatListDirectoryInternal(fs, file, callback);
	tf_fclose(fs, file);
}

// Open a file
FileHandle fatOpenFile(FileSystem* fs, const char* filePath, const char* mode)
{
	return tf_fopen(fs, filePath, mode);
}

// Open a file
FileHandle fatOpenFileFI(FileSystem* fs, const FileInfo* fileInfo, const char* mode)
{
	TFFile* fp = tf_get_free_handle(fs);
	if (fp == NULL)
		return NullFileHandle;

	fp->currentCluster = fileInfo->StartCluster;
	fp->startCluster = fileInfo->StartCluster;
	fp->parentStartCluster = fileInfo->ParentStartCluster;
	fp->size = fileInfo->Size;
	fp->attributes = fileInfo->Attributes;

	fp->currentClusterIdx = 0;
	fp->currentSector = 0;
	fp->currentByte = 0;
	fp->pos = 0;
	fp->flags = TF_FLAG_OPEN;

	if (strchr(mode, 'r'))
	{
		fp->mode |= TF_MODE_READ;
	}
	if (strchr(mode, 'a'))
	{
		tf_unsafe_fseek(fs, fp, fp->size, 0);
		fp->mode |= TF_MODE_WRITE | TF_MODE_OVERWRITE;
	}
	if (strchr(mode, '+'))
	{
		fp->mode |= TF_MODE_OVERWRITE | TF_MODE_WRITE;
	}
	if (strchr(mode, 'w'))
	{
		/* Opened for writing. Truncate file only if it's not a directory*/
		if (!(fp->attributes & TF_ATTR_DIRECTORY))
		{
			fp->size = 0;
			tf_unsafe_fseek(fs, fp, 0, 0);
		
			/* Free the clusterchain starting with the second one if the file
			 * uses more than one */
			u32 cluster;
			if ((cluster = tf_get_fat_entry(fs, fp->startCluster)) != TF_MARK_EOC32)
			{
				tf_free_clusterchain(fs, cluster);
				tf_set_fat_entry(fs, fp->startCluster, TF_MARK_EOC32);
			}
		}
		fp->mode |= TF_MODE_WRITE;
	}

	return fp;
}

// Close an open file
void fatCloseFile(FileSystem* fs, FileHandle file)
{
	tf_fclose(fs, file);
}

// Read data from an open file. Return the number of bytes read
u32 fatReadFile(FileSystem* fs, FileHandle file, u8* outBuffer, u32 length)
{
	return tf_fread(fs, outBuffer, length, file);
}

// Write data to an open file. Return the number of bytes written
u32 fatWriteFile(FileSystem* fs, FileHandle file, const u8* inBuffer, u32 length)
{
	return tf_fwrite(fs, inBuffer, length, file);
}

// Set read/write position in the file. Returns the new position
u32 fatSetFilePos(FileSystem* fs, FileHandle file, u32 pos)
{
	tf_fseek(fs, file, 0, pos);
	return file->pos;
}

// Returns the current read/write position in the file
u32 fatGetFilePos(FileSystem* fs, FileHandle file)
{
	return file->pos;
}

u32 fatGetFileSize(FileSystem* fs, FileHandle file)
{
	return file->pos > file->size ? file->pos : file->size;
}

// Creates all directories and subdirectories in the path
u8 fatCreateDirectory(FileSystem* fs, const char* directoryPath)
{
	tf_mkdir(fs, directoryPath, TRUE);
	return 0;
}

// Creates all directories and subdirectories in the path, relative to the directory pointed by directoryInfo
u8 fatCreateDirectoryFI(FileSystem* fs, FileInfo* directoryInfo, const char* directoryPath)
{
	//SKTD: !!
	return 0;
}

// Deletes a file / directory.
u8 fatDelete(FileSystem* fs, const char* path, bool recursive)
{
	TFFile* Thing = tf_fopen(fs, path, "r+");
	FileInfo Info;
	Info.ParentStartCluster = Thing->parentStartCluster;
	Info.StartCluster = Thing->startCluster;
	Info.Size = Thing->size;
	Info.Attributes = Thing->attributes;
	tf_fclose(fs, Thing);

	return fatDeleteFI(fs, &Info, recursive);
}

// Deletes a file / directory.
u8 fatDeleteFI(FileSystem* fs, FileInfo* info, bool recursive)
{
	//Could probably just check the directory size too, but to be on the safe side we'll do a proper search
	if ((info->Attributes & TF_ATTR_DIRECTORY) == 0)
	{
		//File, just delete
		fatInternal_UnsafeDeleteFI(fs, info);
	}
	else
	{
		//Look for directory entries
		TFFile* fp = fatOpenFileFI(fs, info, "r+");
		while (1)
		{
			FileInfo ChildInfo;
			FatFileEntry CurrentEntry;
			tf_fread(fs, (u8*)&CurrentEntry, sizeof(FatFileEntry), fp);
			u8 FoundEntry = 0;

			while (1)
			{
				if (CurrentEntry.msdos.filename[0] == '\x00')
				{
					break; //No more entries
				}
				else if (CurrentEntry.msdos.filename[0] != (char)0xE5 && CurrentEntry.msdos.attributes != TF_ATTR_VOLUME_LABEL)
				{
					// Found entry, store it and stop looking
					SwizzleFF83(&CurrentEntry.msdos);
					ChildInfo.ParentStartCluster = fp->startCluster;
					ChildInfo.StartCluster = ((u32)(CurrentEntry.msdos.eaIndex & 0xffff) << 16) | (CurrentEntry.msdos.firstCluster & 0xffff);
					ChildInfo.Size = CurrentEntry.msdos.fileSize;

					FoundEntry = 1;
					break;
				}

				//Advance to next entry
				tf_fread(fs, (u8*)&CurrentEntry, sizeof(FatFileEntry), fp);
			}

			tf_fclose(fs, fp);

			if (FoundEntry == 0)
			{
				//Empty directory, just delete it
				fatInternal_UnsafeDeleteFI(fs, info);
				break;
			}
			else if (recursive)
			{
				//Delete the first entry we found
				fatDeleteFI(fs, &ChildInfo, recursive);

				//Then re-open the directory to look for more entries
				fp = fatOpenFileFI(fs, info, "r+");
			}
			else
			{
				//Can't delete a non-empty directory
				break;
			}
		}
	}
	
	return 0;
}

// Gets the cluster length of the open file system
u32 fatGetClusterLength(FileSystem* fs)
{
	return fs->tf_info.sectorsPerCluster * 512; //Bytes per sector always 512
}

// Direct read a cluster
u32 fatReadCluster(FileSystem* fs, u32 cluster, u8 *buffer)
{
	sdReadBlocks(tf_first_sector(fs, cluster), fs->tf_info.sectorsPerCluster, buffer);
	return tf_get_fat_entry(fs, cluster);
}