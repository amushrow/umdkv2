#ifndef __FAT32_H
#define __FAT32_H

#include "types.h"

#define TF_MAX_PATH 256
#define TF_FILE_HANDLES 5

#define TF_FLAG_DIRTY 0x01
#define TF_FLAG_OPEN 0x02
#define TF_FLAG_SIZECHANGED 0x04
#define TF_FLAG_ROOT 0x08

#define TYPE_FAT12 0
#define TYPE_FAT16 1
#define TYPE_FAT32 2

#define TF_MARK_BAD_CLUSTER32 0x0ffffff7
#define TF_MARK_BAD_CLUSTER16 0xfff7
#define TF_MARK_EOC32 0x0ffffff8
#define TF_MARK_EOC16 0xfff8

#define LFN_ENTRY_CAPACITY 13       // bytes per LFN entry

#define TF_ATTR_DIRECTORY 0x10

#define LSN(CN, bpb) SSA + ((CN-2) * bpb->SectorsPerCluster)

#ifndef min
#define min(x,y)  (x<y)? x:y  
#define max(x,y)  (x>y)? x:y  
#endif

	
// Ultimately, once the filesystem is checked for consistency, you only need a few
// things to keep it up and running.  These are:
// 1) The type (fat16 or fat32, no fat12 support)
// 2) The number of sectors per cluster
// 3) Everything needed to compute indices into the FATs, which includes:
//    * Bytes per sector, which is fixed at 512
//    * The number of reserved sectors (pulled directly from the BPB)
// 4) The current sector in memory.  No sense reading it if it's already in memory!

typedef struct struct_tfinfo {
	// FILESYSTEM INFO PROPER
	u8 type; // 0 for FAT16, 1 for FAT32.  FAT12 NOT SUPPORTED
	u8 sectorsPerCluster;
	u32 firstDataSector;
	u32 totalSectors;
	u16 fatBeginAddress;
	// "LIVE" DATA
	u32 currentSector;
	u8 sectorFlags;
	u32 rootDirectorySize;
	u32 rootCluster;
	u32 numSectorsPerFat;
	u8 buffer[512];
} TFInfo;

/////////////////////////////////////////////////////////////////////////////////

typedef struct struct_TFFILE
{
	u32 parentStartCluster;
	u32 startCluster;
	u32 currentClusterIdx;
	u32 currentCluster;
	short currentSector;
	short currentByte;
	u32 pos;
	u8 flags;
	u8 attributes;
	u8 mode;
	u32 size;
	char filename[TF_MAX_PATH];
} TFFile;


#define TF_MODE_READ 0x01
#define TF_MODE_WRITE 0x02
#define TF_MODE_APPEND 0x04
#define TF_MODE_OVERWRITE 0x08

#define TF_ATTR_READ_ONLY 0x01
#define TF_ATTR_HIDDEN 0x02
#define TF_ATTR_SYSTEM 0x04
#define TF_ATTR_VOLUME_LABEL 0x08
#define TF_ATTR_DIRECTORY 0x10
#define TF_ATTR_ARCHIVE 0x20
#define TF_ATTR_DEVICE 0x40 // Should never happen!
#define TF_ATTR_UNUSED 0x80

// New error codes
#define TF_ERR_NO_ERROR 0
#define TF_ERR_BAD_BOOT_SIGNATURE 1
#define TF_ERR_BAD_FS_TYPE 2

#define TF_ERR_INVALID_SEEK 1

// FS Types
#define TF_TYPE_FAT16 0
#define TF_TYPE_FAT32 1

typedef struct _fs
{
	TFInfo tf_info;
	TFFile tf_file_handles[TF_FILE_HANDLES];
} FileSystem;

// New backend functions
int         tf_fetch(FileSystem* fs, u32 sector);
int         tf_store(FileSystem* fs);
u32         tf_get_fat_entry(FileSystem* fs, u32 cluster);
int         tf_set_fat_entry(FileSystem* fs, u32 cluster, u32 value);
int         tf_unsafe_fseek(FileSystem* fs, TFFile *fp, s32 base, long offset);
TFFile*     tf_fnopen(FileSystem* fs, const char *filename, const char *mode, int n);
int         tf_free_clusterchain(FileSystem* fs, u32 cluster);
int         tf_create(FileSystem* fs, const char *filename);
void        tf_release_handle(TFFile *fp);
TFFile*     tf_parent(FileSystem* fs, const char *filename, const char *mode, int mkParents);
int         tf_shorten_filename(char *dest, const char *src, u8 num);

// New frontend functions
u32         tf_get_memory_footprint();
int         tf_init(FileSystem* fs);
int         tf_fflush(FileSystem* fs, TFFile *fp);
int         tf_fseek(FileSystem* fs, TFFile *fp, s32 base, long offset);
int         tf_fclose(FileSystem* fs, TFFile *fp);
u32         tf_fread(FileSystem* fs, u8 *dest, u32 size, TFFile *fp);
int         tf_find_file(FileSystem* fs, TFFile *current_directory, const char *name);
int         tf_compare_filename(FileSystem* fs, TFFile *fp, const char *name);
u32         tf_first_sector(FileSystem* fs, u32 cluster);
const char* tf_walk(FileSystem* fs, const char *filename, TFFile *fp);
TFFile*     tf_fopen(FileSystem* fs, const char *filename, const char *mode);
u32         tf_fwrite(FileSystem* fs, const u8 *src, u32 size, TFFile *fp);
int         tf_fputs(FileSystem* fs, const char *src, TFFile *fp);
int         tf_mkdir(FileSystem* fs, const char *filename, int mkParents);
int         tf_remove(FileSystem* fs, const char *filename);
void        tf_print_open_handles(void);
TFFile*     tf_get_free_handle(FileSystem* fs);
u32         tf_find_free_cluster(FileSystem* fs);
u32         tf_find_free_cluster_from(FileSystem* fs, u32 c);

// User functions
int read_sector(u8 *data, u32 blocknum);
int write_sector(u8 *data, u32 blocknum);

#endif
