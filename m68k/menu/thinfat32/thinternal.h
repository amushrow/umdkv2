#ifndef _THINTERNAL_H
#define _THINTERNAL_H

#define false 0
#define true 1
#define NULL 0

#pragma pack(push, 1)

// Starting at offset 36 into the BPB, this is the structure for a FAT12/16 FS
typedef struct struct_BPBFAT1216_struct {
    u8     BS_DriveNumber;           // 1
    u8     BS_Reserved1;             // 1
    u8     BS_BootSig;               // 1
    u32          BS_VolumeID;              // 4
             u8     BS_VolumeLabel[11];       // 11
         u8     BS_FileSystemType[8];     // 8
} BPB1216_struct;

// Starting at offset 36 into the BPB, this is the structure for a FAT32 FS
typedef struct struct_BPBFAT32_struct {
    u32    FATSize;             // 4
    u16    ExtFlags;              // 2
    u16    FSVersion;             // 2
    u32    RootCluster;           // 4
    u16    FSInfo;                // 2
    u16    BkBootSec;             // 2
    u8     Reserved[12];          // 12
    u8     BS_DriveNumber;            // 1
    u8     BS_Reserved1;              // 1
    u8     BS_BootSig;                // 1
    u32    BS_VolumeID;               // 4
    u8     BS_VolumeLabel[11];        // 11
    u8     BS_FileSystemType[8];      // 8
} BPB32_struct;

typedef struct struct_BPB_struct {
    u8     BS_JumpBoot[3];            // 3
    u8     BS_OEMName[8];             // 8
    u16    BytesPerSector;        // 2
    u8     SectorsPerCluster;     // 1
    u16    ReservedSectorCount;   // 2
    u8     NumFATs;               // 1
    u16    RootEntryCount;        // 2
    u16    TotalSectors16;        // 2
    u8     Media;                 // 1
    u16    FATSize16;             // 2
    u16    SectorsPerTrack;       // 2
    u16    NumberOfHeads;         // 2
    u32          HiddenSectors;         // 4
    u32          TotalSectors32;        // 4
    union {
        BPB1216_struct fat16;
        BPB32_struct fat32;
    } FSTypeSpecificData;
} BPB_struct;

typedef struct struct_FatFile83 {
    char filename[8];
    char extension[3];
    u8 attributes;
    u8 reserved;
    u8 creationTimeMs;
    u16 creationTime;
    u16 creationDate;
    u16 lastAccessTime;
    u16 eaIndex;
    u16 modifiedTime;
    u16 modifiedDate;
    u16 firstCluster;
    u32 fileSize;
} FatFile83;

typedef struct struct_FatFileLFN {
    u8 sequence_number;
    u16 name1[5];      // 5 Chars of name (UTF 16???)
    u8 attributes;     // Always 0x0f
    u8 reserved;       // Always 0x00
    u8 checksum;       // Checksum of DOS Filename.  See Docs.
    u16 name2[6];      // 6 More chars of name (UTF-16)
        u16 firstCluster;  // Always 0x0000
    u16 name3[2];
} FatFileLFN;

typedef union struct_FatFileEntry {
    FatFile83 msdos;
    FatFileLFN lfn;
} FatFileEntry;

#pragma pack(pop)

void Swizzle16(u16* val);
void Swizzle32(u32* val);
void SwizzleBPB32(BPB32_struct* bpb32);
void SwizzleBPB(BPB_struct* bpb);
void SwizzleFF83(FatFile83* ff);
void SwizzleFFLFN(FatFileLFN* ff);

// "Legacy" functions
u32 fat_size(BPB_struct *bpb);
int total_sectors(BPB_struct *bpb);
int root_dir_sectors(BPB_struct *bpb);
int cluster_count(BPB_struct *bpb);
int fat_type(BPB_struct *bpb);
int first_data_sector(BPB_struct *bpb);
int first_sector_of_cluster(BPB_struct *bpb, int N);
int data_sectors(BPB_struct *bpb);
int fat_sector_number(BPB_struct *bpb, int N);
int fat_entry_offset(BPB_struct *bpb, int N);
int fat_entry_for_cluster(BPB_struct *bpb, u8 *buffer, int N);

#endif
