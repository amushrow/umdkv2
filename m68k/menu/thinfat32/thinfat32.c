#include "thinfat32.h"
#include "thinternal.h"
#include "types.h"
#include "memory.h"
#include "string.h"
#include "../sdcard.h"

// USERLAND
int read_sector(u8 *data, u32 sector)
{
	sdReadBlocks(sector, 1, data);
	return 0;
}

int write_sector(u8 *data, u32 blocknum)
{
	sdWriteBlocks(blocknum, 1, data);
	return 0;
}

/*
 * Fetch a single sector from disk.
 * ARGS
 *   sector - the sector number to fetch.
 * SIDE EFFECTS
 *   tf_info->buffer contains the 512 byte sector requested
 *   tf_info->currentSector contains the sector number retrieved
 *   if tf_info->buffer already contained a fetched sector, and was marked dirty, that sector is
 *   tf_store()d back to its appropriate location before executing the fetch.
 * RETURN
 *   the return code given by the users read_sector() (should be zero for NO ERROR, nonzero otherwise)
 */
int tf_fetch(FileSystem* fs, u32 sector)
{
	int rc=0;

	// Don't actually do the fetch if we already have it in memory
	if(sector == fs->tf_info.currentSector) 
		return 0;
	
	// If the sector we already have prefetched is dirty, write it before reading out the new one
	if(fs->tf_info.sectorFlags & TF_FLAG_DIRTY)
		rc |= tf_store(fs);
	
	// Do the read, pass up the error flag
	rc |= read_sector( fs->tf_info.buffer, sector );
	
	if(!rc) 
		fs->tf_info.currentSector = sector;

	return rc;
}

/*
 * Store the current sector back to disk
 * SIDE EFFECTS
 *   512 bytes of tf_info->buffer are stored on disk in the sector specified by tf_info->currentSector
 * RETURN
 *   the error code given by the users write_sector() (should be zero for NO ERROR, nonzero otherwise)
 */
int tf_store(FileSystem* fs)
{
	fs->tf_info.sectorFlags &= ~TF_FLAG_DIRTY;
	return write_sector( fs->tf_info.buffer, fs->tf_info.currentSector );
}

u32 tf_get_memory_footprint()
{
	return sizeof(FileSystem);
}

/*
 * Initialize the filesystem
 * Reads filesystem info from disk into tf_info and checks that info for validity
 * SIDE EFFECTS
 *   Sector 0 is fetched into tf_info->buffer
 *   If TF_DEBUG is specified tf_stats is initialized
 * RETURN
 *   0 for a successfully initialized filesystem, nonzero otherwise.
 */
int tf_init(FileSystem* fs)
{
	for (u16 i = 0; i < TF_FILE_HANDLES; ++i)
		fs->tf_file_handles[i].flags = 0;

	BPB_struct *bpb;
	u32 fat_size, root_dir_sectors, data_sectors, cluster_count, temp;
	TFFile *fp;
	FatFileEntry e;

	u32 VIDAddress = 0;
	{
		sdReadBlocks(0, 1, fs->tf_info.buffer);
		if ((fs->tf_info.buffer[446 + 4] == 0x0C || fs->tf_info.buffer[446 + 4] == 0x0B) &&
			fs->tf_info.buffer[510] == 0x55 && fs->tf_info.buffer[511] == 0xAA)
		{
			VIDAddress = *((int*)(fs->tf_info.buffer + 454));
			Swizzle32(&VIDAddress);
		}
		else
		{
			return TF_ERR_BAD_FS_TYPE;
		}
	}


	// Initialize the runtime portion of the TFInfo structure, and read sec0
	fs->tf_info.currentSector = -1;
	fs->tf_info.sectorFlags = 0;
	tf_fetch(fs, VIDAddress);

	// Cast to a BPB, so we can extract relevant data
	bpb = (BPB_struct *) fs->tf_info.buffer;
	SwizzleBPB(bpb);
	
	/* Only specific bytes per sector values are allowed
	 * FIXME: Only 512 bytes are supported by thinfat at the moment */
	if (bpb->BytesPerSector != 512)
		return TF_ERR_BAD_FS_TYPE;

	if (bpb->ReservedSectorCount == 0)
		return TF_ERR_BAD_FS_TYPE;

	/* Valid media types */
	if ((bpb->Media != 0xF0) && ((bpb->Media < 0xF8) || (bpb->Media > 0xFF)))
		return TF_ERR_BAD_FS_TYPE;

	// See the FAT32 SPEC for how this is all computed
	fat_size                   = (bpb->FATSize16 != 0) ? bpb->FATSize16 : bpb->FSTypeSpecificData.fat32.FATSize;
	root_dir_sectors           = ((bpb->RootEntryCount*32) + (bpb->BytesPerSector-1))/(512); // The 512 here is a hardcoded bpb->bytesPerSector (TODO: Replace /,* with shifts?)
	fs->tf_info.totalSectors       = (bpb->TotalSectors16 != 0) ? bpb->TotalSectors16 : bpb->TotalSectors32;
	data_sectors               = fs->tf_info.totalSectors - (bpb->ReservedSectorCount + (bpb->NumFATs * fat_size) + root_dir_sectors);
	fs->tf_info.sectorsPerCluster  = bpb->SectorsPerCluster;
	cluster_count              = data_sectors/fs->tf_info.sectorsPerCluster;
	fs->tf_info.fatBeginAddress    = VIDAddress + bpb->ReservedSectorCount;
	fs->tf_info.firstDataSector    = VIDAddress + bpb->ReservedSectorCount + (bpb->NumFATs * fat_size) + root_dir_sectors;

	// Now that we know the total count of clusters, we can compute the FAT type
	if(cluster_count < 65525)
		return TF_ERR_BAD_FS_TYPE;
	else
		fs->tf_info.type = TF_TYPE_FAT32;

	fs->tf_info.rootCluster = bpb->FSTypeSpecificData.fat32.RootCluster;
	fs->tf_info.numSectorsPerFat = bpb->FSTypeSpecificData.fat32.FATSize;

	// TODO ADD SANITY CHECKING HERE (CHECK THE BOOT SIGNATURE, ETC... ETC...)
	fs->tf_info.rootDirectorySize = 0xffffffff;
	temp = 0;

	// Like recording the root directory size!
	// TODO, THis probably isn't necessary.  Remove later
	fp = tf_fopen(fs, "/", "r");
	do
	{
		temp += sizeof(FatFileEntry);
		tf_fread(fs, (u8*)&e, sizeof(FatFileEntry), fp);
	} while(e.msdos.filename[0] != '\x00');
	
	tf_fclose(fs, fp);
	fs->tf_info.rootDirectorySize = temp;

	return 0;
}

/*
 * Return the FAT entry for the given cluster
 * ARGS
 *   cluster - The cluster number for the requested FAT entry
 * SIDE EFFECTS
 *   Retreives whatever sector happens to contain that FAT entry (if it's not already in memory)
 * RETURN
 *   The value of the fat entry for the specified cluster.
 */
u32 tf_get_fat_entry(FileSystem* fs, u32 cluster)
{
	u32 offset=cluster*4;
	tf_fetch(fs, fs->tf_info.fatBeginAddress + (offset/512)); // 512 is hardcoded bpb->bytesPerSector

	u32 nextCluster = *((u32 *) &(fs->tf_info.buffer[offset % 512]));
	Swizzle32(&nextCluster);
	return nextCluster;
}

/*
 * Sets the fat entry on disk for a given cluster to the specified value.
 * ARGS
 *   cluster - The cluster number for which to set the FAT entry
 *     value - The new value for the FAT entry  
 * SIDE EFFECTS
 *   Fetches whatever sector happens to contain the pertinent fat entry (if it's not already in memory)
 * RETURN
 *   0 for no error, or nonzero for error with fetch
 * TODO
 *   Does the sector modified here need to be flagged as dirty?
 */
int tf_set_fat_entry(FileSystem* fs, u32 cluster, u32 value)
{
	u32 offset;
	int rc;

	offset=cluster*4; // FAT32
	rc = tf_fetch(fs, fs->tf_info.fatBeginAddress + (offset/512)); // 512 is hardcoded bpb->bytesPerSector
	u32 swizzledValue = value;
	Swizzle32(&swizzledValue);
	if (*((u32 *) &(fs->tf_info.buffer[offset % 512])) != swizzledValue)
	{
		fs->tf_info.sectorFlags |= TF_FLAG_DIRTY; // Mark this sector as dirty
		*((u32 *) &(fs->tf_info.buffer[offset % 512])) = swizzledValue;
	}
	return rc;
}


/*
 * Return the index of the first sector for the provided cluster
 * ARGS
 *   cluster - The cluster of interest
 * RETURN
 *   The first sector of the provided cluster
 */
u32 tf_first_sector(FileSystem* fs, u32 cluster)
{
	return ((cluster-2)*fs->tf_info.sectorsPerCluster) + fs->tf_info.firstDataSector;
}

/*
 * Walks the path provided, returning a valid file pointer for each successive level in the path.
 *
 * example:  tf_walk("/home/ryan/myfile.txt", fp to "/")
 *           Call once: returns pointer to string: home/ryan/myfile.txt, fp now points to directory for /
 *          Call again: returns pointer to string: ryan/myfile.txt, fp now points to directory for /home
 *          Call again: returns pointer to string: myfile.txt, fp now points to directory for /home/ryan
 *          Call again: returns pointer to the end of the string, fp now points to /home/ryan/myfile.txt
 *          Call again: returns NULL pointer. fp is unchanged
 * ARGS
 *   filename - a string containing the full path
 *
 * SIDE EFFECTS
 *   The filesystem is traversed, so files are opened and closed, sectors are read, etc...
 * RETURN
 *   A string pointer to the next level in the path, or NULL if this is the end of the path
 */
const char *tf_walk(FileSystem* fs, const char *filename, TFFile *fp)
{
	FatFileEntry entry;
	
	// We're out of path. this walk is COMPLETE
	if(*filename == '/')
	{
		filename++;
		if(*filename == '\x00') return NULL;
	}
	// There's some path left
	if(*filename != '\x00')
	{
		// fp is the handle for the current directory
		// filename is the name of the current file in that directory
		// Go fetch the FatFileEntry that corresponds to the current file
		// Remember that tf_find_file is only going to search from the beginning of the filename
		// up until the first path separation character
		if(tf_find_file(fs, fp, filename))
		{
			// This happens when we couldn't actually find the file
			fp->flags = 0xff;
			return NULL;
		}
		tf_fread(fs, (u8*)&entry, sizeof(FatFileEntry), fp);
		SwizzleFF83(&entry.msdos);

		// Walk over path separators
		while((*filename != '/') && (*filename != '\x00'))
			filename+=1;

		if(*filename == '/')
			filename +=1 ;

		// Set up the file pointer now that we've got information for the next level in the path hierarchy
		fp->parentStartCluster = fp->startCluster;
		fp->startCluster = ((u32)(entry.msdos.eaIndex & 0xffff) << 16) | (entry.msdos.firstCluster & 0xffff);
		if (fp->startCluster == 0)
			fp->startCluster = fs->tf_info.rootCluster;

		fp->attributes = entry.msdos.attributes;
		fp->currentCluster = fp->startCluster;
		fp->currentClusterIdx=0;
		fp->currentSector=0;
		fp->currentByte=0;
		fp->pos=0;
		fp->flags=TF_FLAG_OPEN;
		fp->size=(entry.msdos.attributes & TF_ATTR_DIRECTORY) ? 0xffffffff :entry.msdos.fileSize;
		if(*filename == '\x00')
			return NULL;

		return filename;
	}

	// We're out of path.  This walk is COMPLETE.
	return NULL;
}

/*
 * Searches the list of system file handles for a free one, and returns it.
 * RETURN
 *   NULL if no system file handles are free, or the free handle if one is available.
 */
TFFile *tf_get_free_handle(FileSystem* fs)
{
	int i;
	TFFile *fp;
	for(i=0; i<TF_FILE_HANDLES; i++)
	{
		fp = &fs->tf_file_handles[i];
		if(fp->flags & TF_FLAG_OPEN)
			continue;

		// We get here if we find a free handle
		fp->flags = TF_FLAG_OPEN;
		return fp;
	}

	return NULL;
}

/*
 * Release a filesystem handle (mark as available)
 */
void tf_release_handle(TFFile *fp)
{
	fp->flags &= ~TF_FLAG_OPEN;
}

// Convert a character to uppercase
// TODO: Re-do how filename conversions are done.
u8 upper(u8 c)
{
	if(c >= 'a' && c <= 'z')
		return c + ('A'-'a');
	else
		return c;
}

void tf_choose_sfn(FileSystem* fs, char *dest, const char *src, TFFile *fp)
{
	char temp[13];
	int results, num = 1;
	TFFile xfile;
	// throwaway fp that doesn't muck with the original
	memcpy( &xfile, fp, sizeof(TFFile) );
	
	while (1)
	{
		results = tf_shorten_filename( dest, src, num );
		switch (results)
		{
		case 0: // ok
			// does the file collide with the current directory?
			//tf_fseek(xfile, 0, 0);
			memcpy(temp, dest, 8);
			memcpy(temp+9, dest+8, 3);
			temp[8] = '.';
			temp[12] = 0;
			
			if (0 > tf_find_file(fs, &xfile, temp ) )
			{
				//tf_
				return;
				break;
			}
			//tf
			num++;
			break;
			
		case -1: // error
			return;
		}
	}
}

/*
 * Take the long filename (filename only, not full path) specified by src,
 * and convert it to an 8.3 compatible filename, storing the result at dst
 * TODO: This should return something, an error code for conversion failure.
 * TODO: This should handle special chars etc.
 * TODO: Test for short filenames, (<7 characters)
 * TODO: Modify this to use the basis name generation algorithm described in the FAT32 whitepaper.
 */
int tf_shorten_filename(char *dest, const char *src, u8 num)
{
	int i;
	const char *tmp;
	
	// strip through and chop special chars

	tmp = strrchr(src, '.');
	// copy the extension
	for (i=0; i<3; i++)
	{
		while (tmp!=0 && *tmp!= 0 && !(*tmp <0x7f && *tmp>20 
			   && *tmp !=0x22
			   && *tmp !=0x2a
			   && *tmp !=0x2e
			   && *tmp !=0x2f
			   && *tmp !=0x3a
			   && *tmp !=0x3c
			   && *tmp !=0x3e
			   && *tmp !=0x3f
			   && *tmp !=0x5b
			   && *tmp !=0x5c
			   && *tmp !=0x5d
			   && *tmp !=0x7c))
			tmp++;
		if (tmp==0 || *tmp == 0)
			*(dest+8+i) = ' ';
		else
			*(dest+8+i) = upper(*(tmp++));
	}
	
	// Copy the basename
	i=0;
	tmp = strrchr(src, '.');    
	while(1) {
		if (i==8) break;
		if (src == tmp)
		{
			dest[i++] = ' ';
			continue;
		}
		
		if((*dest != ' ')) {
		while (*src!= 0 && !(*src <0x7f && *src>20 
			   && *src !=0x22
			   && *src !=0x2a
			   && *src !=0x2e
			   && *src !=0x2f
			   && *src !=0x3a
			   && *src !=0x3c
			   && *src !=0x3e
			   && *src !=0x3f
			   && *src !=0x5b
			   && *src !=0x5c
			   && *src !=0x5d
			   && *src !=0x7c))
			src++;
		if (*src == 0)
			dest[i] = ' ';
		else if ( *src==','
				 || *src == '['
					 || *src == ']')
			dest[i] = '_';
		else
			dest[i] = upper(*(src++));
		}
		i+=1;
	}    
	// now that they are populated, do analysis.
	// if num>4, do 2 letters
	if (num > 4)
	{
		intToHex(num, dest + 2, 4);
		dest[6] = '~';
		dest[7] = '1';
	}
	else
	{
		tmp = strchr(dest, ' ');
		if (tmp==0 || tmp-dest > 6)
		{
			dest[6] = '~';
			dest[7] = num + 0x30;
		}
		else
		{
			//--SKTD: What is this?
			//*tmp++ = '~';
			//*tmp++ = num + 0x30;
		}
	}
   
	return 0;
}

/*
 * Create a LFN entry from the filename provided.
 * - The entry is constructed from all, or the first 13 characters in the filename (whichever is smaller)
 * - If filename is <=13 bytes long, the NULL pointer is returned
 * - If the filename >13 bytes long, an entry is constructed for the first 13 bytes, and a pointer is 
 *   returned to the remainder of the filename.
 * ARGS
 *   filename - string containing the filename for which to create an entry
 *   entry - pointer to a FatFileEntry structure, which is populated as an LFN entry
 * RETURN
 *   NULL if this is the last entry for this file, or a string pointer to the remainder of the string
 *   if the entire filename won't fit in one entry
 * WARNING
 *   Because this function works in forward order, and LFN entries are stored in reverse, it does
 *   NOT COMPUTE LFN SEQUENCE NUMBERS.  You have to do that on your own.  Also, because the function
 *   acts on partial filenames IT DOES NOT COMPUTE LFN CHECKSUMS.  You also have to do that on your own.  
 * TODO
 *   Test and further improve on this function
 */
const char* tf_create_lfn_entry(const char *filename, FatFileEntry *entry)
{
	int i, done=0;
	for(i=0; i<5; i++)
	{
		if (!done)
			entry->lfn.name1[i] = (unsigned short) *(filename);
		else
			entry->lfn.name1[i] = 0xffff;
	
		if(*filename++ == '\x00')
			done = 1;
	}
	
	for(i=0; i<6; i++)
	{
		if (!done)
			entry->lfn.name2[i] = (unsigned short) *(filename);
		else
			entry->lfn.name2[i] = 0xffff;
	
		if(*filename++ == '\x00')
			done = 1;
	}

	for(i=0; i<2; i++)
	{
		if (!done)
			entry->lfn.name3[i] = (unsigned short) *(filename);
		else
			entry->lfn.name3[i] = 0xffff;

		if(*filename++ == '\x00')
			done = 1;
	}
		
	entry->lfn.attributes = 0x0f;
	entry->lfn.reserved = 0;
	entry->lfn.firstCluster = 0;
	
	if(done)
		return NULL;
	
	if(*filename)
		return filename;

	return NULL;
}
// Taken from http://en.wikipedia.org/wiki/File_Allocation_Table
//
u8 tf_lfn_checksum(const char *pFcbName)
{
	int i;
	u8 sum=0;
 
	for (i=11; i; i--)
		sum = ((sum & 1) << 7) + (sum >> 1) + *pFcbName++;
	return sum;
}

int tf_place_lfn_chain(FileSystem* fs, TFFile *fp, const char* filename, const char* sfn)
{
	// Windows does reverse chaining:  0x44, 0x03, 0x02, 0x01
	const char* strptr = filename;
	int entries=1;
	int i;
	const char* last_strptr = filename;
	FatFileEntry entry;
	u8 seq;

	// create the chains - probably only to get a count!?
	// FIXME: just pre-calculate and don't do all this recomputing!
	while((strptr = tf_create_lfn_entry(strptr, &entry)))
	{
		last_strptr = strptr;
		entries += 1;
	}
	
	// LFN sequence number (first byte of LFN)
	seq = entries | 0x40;
	for(i=0; i<entries; i++)
	{
		tf_create_lfn_entry(last_strptr, &entry);
		entry.lfn.sequence_number = seq;
		entry.lfn.checksum = tf_lfn_checksum(sfn);
		
		SwizzleFFLFN(&entry.lfn);
		tf_fwrite(fs, (u8*)&entry, sizeof(FatFileEntry), fp);
		SwizzleFFLFN(&entry.lfn);
		seq = ((seq & ~0x40)-1);
		last_strptr -= 13;    
	}
	return 0;
}

int tf_create(FileSystem* fs, const char *filename)
{
	TFFile *fp = tf_parent(fs, filename, "r", false);
	FatFileEntry entry;
	u32 cluster;
	const char* temp;

	if(!fp) return 1;
	tf_fclose(fs, fp);
	fp = tf_parent(fs, filename, "r+", false);
	
	// Now we have the directory in which we want to create the file, open for overwrite
	do
	{
		//"seek" to the end
		tf_fread(fs, (u8*)&entry, sizeof(FatFileEntry), fp);
	} while(entry.msdos.filename[0] != '\x00');

	// Back up one entry, this is where we put the new filename entry
	tf_fseek(fs, fp, -sizeof(FatFileEntry), fp->pos);
	cluster = tf_find_free_cluster(fs);
	tf_set_fat_entry(fs, cluster, TF_MARK_EOC32); // Marks the new cluster as the last one (but no longer free)
	
	entry.msdos.attributes = 0;
	entry.msdos.creationTimeMs = 0x25;
	entry.msdos.creationTime = 0x7e3c;
	entry.msdos.creationDate = 0x4262;
	entry.msdos.lastAccessTime = 0x4262;
	entry.msdos.eaIndex = (cluster >> 16) & 0xffff;
	entry.msdos.modifiedTime = 0x7e3c;
	entry.msdos.modifiedDate = 0x4262;
	entry.msdos.firstCluster = cluster & 0xffff;
	entry.msdos.fileSize = 0;
	temp = strrchr(filename, '/')+1;
	
	tf_choose_sfn(fs, entry.msdos.filename, temp, fp);
	tf_place_lfn_chain(fs, fp, temp, entry.msdos.filename);
	
	SwizzleFF83(&entry.msdos);
	tf_fwrite(fs, (u8*)&entry, sizeof(FatFileEntry), fp);

	memset(&entry, 0, sizeof(FatFileEntry));
	tf_fwrite(fs, (u8*)&entry, sizeof(FatFileEntry), fp);

	tf_fclose(fs, fp);

	return 0;
}

/* tf_mkdir attempts to create a new directory in the filesystem.  duplicates
are *not* allowed!

returns 1 on failure
returns 0 on success
*/
int tf_mkdir(FileSystem* fs, const char *filename, int mkParents)
{
	// FIXME: verify that we can't create multiples of the same one.
	// FIXME: figure out how the root directory location is determined.
	char orig_fn[ TF_MAX_PATH ];
	TFFile *fp;
	FatFileEntry entry, blank;

	u32 cluster;
	u32 parentCluster;
	const char* temp;

	strncpy( (char*)orig_fn, (const char*)filename, TF_MAX_PATH-1 );
	orig_fn[ TF_MAX_PATH-1 ] = 0;
	
	memset(&blank, 0, sizeof(FatFileEntry));
	
	fp = tf_fopen(fs, filename, "r");
	if (fp)  // if not NULL, the filename already exists.
	{
		tf_fclose(fs, fp);
		tf_release_handle(fp);
		if (mkParents)
		{
			return 0;
		}
		
		return 1;
	}
	
	fp = tf_parent(fs, filename, "r+", mkParents);
	if (!fp)
	{
		return 1;
	}
	parentCluster = fp->startCluster;

	// Now we have the directory in which we want to create the file, open for overwrite
	do
	{
		tf_fread(fs, (u8*)&entry, sizeof(FatFileEntry), fp);
	} while(entry.msdos.filename[0] != '\x00');
	
	// Back up one entry, this is where we put the new filename entry
	tf_fseek(fs, fp, -sizeof(FatFileEntry), fp->pos);
	
	// go find some space for our new friend
	cluster = tf_find_free_cluster(fs);
	tf_set_fat_entry(fs, cluster, TF_MARK_EOC32); // Marks the new cluster as the last one (but no longer free)
	
	// set up our new directory entry
	// TODO shorten these entries with memset
	entry.msdos.attributes = TF_ATTR_DIRECTORY ;
	entry.msdos.creationTimeMs = 0x25;
	entry.msdos.creationTime = 0x7e3c;
	entry.msdos.creationDate = 0x4262;
	entry.msdos.lastAccessTime = 0x4262;
	entry.msdos.eaIndex = (cluster >> 16) & 0xffff;
	entry.msdos.modifiedTime = 0x7e3c;
	entry.msdos.modifiedDate = 0x4262;
	entry.msdos.firstCluster = cluster & 0xffff;
	entry.msdos.fileSize = 0;
	temp = strrchr(filename, '/')+1;

	tf_choose_sfn(fs, entry.msdos.filename, temp, fp);
	tf_place_lfn_chain(fs, fp, temp, entry.msdos.filename);

	SwizzleFF83(&entry.msdos);
	tf_fwrite(fs, (u8*)&entry, sizeof(FatFileEntry), fp);
	
	// placing a 0 at the end of the FAT
	tf_fwrite(fs, (u8*)&blank, sizeof(FatFileEntry), fp);
	tf_fclose(fs, fp);
	
	fp = tf_fopen(fs, orig_fn, "w");
	
	// set up .
	memcpy( entry.msdos.filename, ".          ", 11 );
	tf_fwrite(fs, (u8*)&entry, sizeof(FatFileEntry), fp);
	
	// set up ..
	memcpy( entry.msdos.filename, "..         ", 11 );
	SwizzleFF83(&entry.msdos);
	entry.msdos.eaIndex = (parentCluster >> 16) & 0xffff;
	entry.msdos.firstCluster = (parentCluster & 0xffff);
	SwizzleFF83(&entry.msdos);
	tf_fwrite(fs, (u8*)&entry, sizeof(FatFileEntry), fp);

	// placing a 0 at the end of the FAT
	tf_fwrite(fs, (u8*)&blank, sizeof(FatFileEntry), fp);
	
	tf_fclose(fs, fp);
	tf_release_handle(fp);
	return 0;
}

TFFile *tf_fopen(FileSystem* fs, const char *filename, const char *mode)
{
	TFFile *fp;

	fp = tf_fnopen(fs, filename, mode, strlen((char*)filename));
	if(fp == NULL)
	{
		if(strchr(mode, '+') || strchr(mode, 'w') || strchr(mode, 'a'))
			tf_create(fs, filename); 
		
		return tf_fnopen(fs, filename, mode, strlen((char*)filename));
	}
	return fp;
}

//
// Just like fopen, but only look at n u8acters of the path
TFFile *tf_fnopen(FileSystem* fs, const char *filename, const char *mode, int n)
{
	// Request a new file handle from the system
	TFFile *fp = tf_get_free_handle(fs);
	char myfile[256];
	const char* temp_filename = myfile;
	u32 cluster;

	if (fp == NULL)
		return NULL;

	strncpy((char*)myfile, (char*)filename, n);
	myfile[n] = 0;
	fp->currentCluster = fs->tf_info.rootCluster;
	fp->startCluster = fs->tf_info.rootCluster;
	fp->parentStartCluster=0xffffffff;
	fp->currentClusterIdx=0;
	fp->currentSector=0;
	fp->currentByte=0;
	fp->attributes = TF_ATTR_DIRECTORY;
	fp->pos=0;
	fp->flags |= TF_FLAG_ROOT;
	fp->size = 0xffffffff;
	fp->mode=TF_MODE_READ | TF_MODE_WRITE | TF_MODE_OVERWRITE;

	while(temp_filename != NULL)
	{
		temp_filename = tf_walk(fs, temp_filename, fp);
		if(fp->flags == 0xff)
		{
			tf_release_handle(fp);
			return NULL;
		}
	}
	
	if(strchr(mode, 'r'))
		fp->mode |= TF_MODE_READ;
	
	if(strchr(mode, 'a'))
	{
		tf_unsafe_fseek(fs, fp, fp->size, 0);
		fp->mode |= TF_MODE_WRITE | TF_MODE_OVERWRITE;
	}

	if(strchr(mode, '+'))
		fp->mode |= TF_MODE_OVERWRITE | TF_MODE_WRITE;

	if(strchr(mode, 'w'))
	{
		/* Opened for writing. Truncate file only if it's not a directory*/
		if (!(fp->attributes & TF_ATTR_DIRECTORY))
		{
			fp->size = 0;
			tf_unsafe_fseek(fs, fp, 0, 0);
			
			/* Free the clusterchain starting with the second one if the file
			 * uses more than one */
			if ((cluster = tf_get_fat_entry(fs, fp->startCluster)) != TF_MARK_EOC32)
			{
				tf_free_clusterchain(fs, cluster);
				tf_set_fat_entry(fs, fp->startCluster, TF_MARK_EOC32);
			}
		}
		fp->mode |= TF_MODE_WRITE;
	}

	strncpy((char*)fp->filename, (char*)myfile, n);
		 
	fp->filename[n] = 0;
	return fp;
}

int tf_free_clusterchain(FileSystem* fs, u32 cluster)
{
	u32 fat_entry;
	fat_entry = tf_get_fat_entry(fs, cluster);
	while(fat_entry < TF_MARK_EOC32)
	{
		if (fat_entry <= 2)        // catch-all to save root directory from corrupted stuff
		{
			break;
		}

		tf_set_fat_entry(fs, cluster, 0x00000000);
		fat_entry = tf_get_fat_entry(fs, fat_entry);
		cluster = fat_entry;
	}

	return 0;
}



int tf_fseek(FileSystem* fs, TFFile *fp, s32 base, long offset)
{
	long pos = base+offset;
	if (pos >= fp->size) return TF_ERR_INVALID_SEEK;
	return tf_unsafe_fseek(fs, fp, base, offset); 
}

/*
 * TODO: Make it so seek fails aren't destructive to the file handle
 */
int tf_unsafe_fseek(FileSystem* fs, TFFile *fp, s32 base, long offset)
{
	u32 cluster_idx;
	long pos = base + offset;
	u32 mark = fs->tf_info.type ? TF_MARK_EOC32 : TF_MARK_EOC16;
	u32 temp;
	// We're only allowed to seek one past the end of the file (For writing new stuff)
	if(pos > fp->size)
		return TF_ERR_INVALID_SEEK;

	if(pos == fp->size)
	{
		fp->size += 1;
		fp->flags |= TF_FLAG_SIZECHANGED;
	}
	
	// Compute the cluster index of the new location
	cluster_idx = pos / (fs->tf_info.sectorsPerCluster*512); // The cluster we want in the file

	// If the cluster index matches the index we're already at, we don't need to look in the FAT
	// If it doesn't match, we have to follow the linked list to arrive at the correct cluster 
	if(cluster_idx != fp->currentClusterIdx)
	{
		temp = cluster_idx;
	
		/* Shortcut: If we are looking for a cluster that comes *after* the current we don't
		 * need to start at the beginning */
		if (cluster_idx > fp->currentClusterIdx)
			cluster_idx -= fp->currentClusterIdx;
		else
			fp->currentCluster = fp->startCluster;
		
		fp->currentClusterIdx = temp;
		while(cluster_idx > 0)
		{
			// TODO Check file mode here for r/w/a/etc...
			temp = tf_get_fat_entry(fs, fp->currentCluster); // next, next, next
			if ((temp & 0x0fffffff) != mark)
			{
				fp->currentCluster = temp;
			}
			else
			{
				// We've reached the last cluster in the file (omg)
				// If the file is writable, we have to allocate new space
				// If the file isn't, our job is easy, just report an error
				// Also, probably report an error if we're out of space
				temp = tf_find_free_cluster_from(fs, fp->currentCluster);
				tf_set_fat_entry(fs, fp->currentCluster, temp); // Allocates new space
				tf_set_fat_entry(fs, temp, mark); // Marks the new cluster as the last one
				fp->currentCluster = temp;
			}

			cluster_idx--;
			if(fp->currentCluster >= mark)
			{
				if (cluster_idx > 0)
					return TF_ERR_INVALID_SEEK;
			}
		}
		// We now have the correct cluster number (whether we had to fetch it from the fat, or realized we already had it)
		// Now we need just compute the correct sector and byte index into the cluster
	}

	fp->currentByte = pos % (fs->tf_info.sectorsPerCluster*512); // The offset into the cluster
	fp->pos = pos;
	return 0;
}

/*
 * Given a file handle to the current directory and a filename, populate the provided FatFileEntry with the
 * file information for the given file.
 * SIDE EFFECT: the position in current_directory will be set to the beginning of the fatfile entry (for overwriting purposes)
 * returns 0 on match, -1 on fail
 */
int tf_find_file(FileSystem* fs, TFFile *current_directory, const char *name)
{
	int rc;
	tf_fseek(fs, current_directory, 0, 0);
	
	while(1)
	{
		rc = tf_compare_filename(fs, current_directory, name);
		if(rc < 0)
			break;
		else if(rc == 1)
			return 0; // found!
	}

	return -1;
}

/*! tf_compare_filename_segment compares a given filename against a particular
FatFileEntry (a 32-byte structure pulled off disk, all of these are back-to-back
in a typical Directory entry on the disk)

figures out formatted name, does comparison, and returns 0:failure, 1:match
*/
int tf_compare_filename_segment(FatFileEntry *entry, const char *name)
{
	int i,j;
	char reformatted_file[16];
	const char* entryname = entry->msdos.filename;

	if(entry->msdos.attributes != 0x0f)
	{
		// Filename
		j=0;
		for(i=0; i<8; i++)
		{
			if(entryname[i] != ' ')
				reformatted_file[j++] = entryname[i];
		}

		if (entryname[8] != ' ')
		{
			// Extension
			reformatted_file[j++] = '.';
	
			for (i = 8; i < 11; i++)
			{
				if (entryname[i] != ' ')
					reformatted_file[j++] = entryname[i];
			}
		}
	}
	else
	{
		j=0;
		for(i=0; i<5; i++)
			reformatted_file[j++] = (u8) entry->lfn.name1[i];
		
		for(i=0; i<6; i++)
			reformatted_file[j++] = (u8) entry->lfn.name2[i];

		for(i=0; i<2; i++)
			reformatted_file[j++] = (u8) entry->lfn.name3[i];
	}
	reformatted_file[j++] = '\x00';

	i=0;
	while((name[i] != '/') && (name[i] != '\x00'))
		i++; // will this work for 8.3?  this should be calculated in the section with knowledge of lfn/8.3
	
	// FIXME: only compare the 13 or less bytes left in the reformatted_file string... but that doesn't match all the way to the end of the test string....
	
	// the role of 'i' changes here to become the return value.  perhaps this doesn't gain us enough in performance to avoid using a real retval?
	/// PROBLEM: THIS FUNCTION assumes that if the length of the "name" is tested by the caller.
	///   if the LFN pieces all match, but the "name" is longer... this will never fail.
	if (i>13)
	{
		if (strncasecmp(name, reformatted_file, 13)) 
			i = 0;
		else
			i = 1;
	}
	else
	{ 
		if (reformatted_file[i] != 0 || strncasecmp((char*)name, (char*)reformatted_file, i))
			i = 0;
		else
			i = 1;
	}

	return i;
}

// 
// Reads a single FatFileEntry from fp, compares it to the MSDOS filename specified by *name
// Returns:
//   1 for entry matches filename.  Side effect: fp seeks to that entry
//   0 for entry doesn't match filename.  Side effect: fp seeks to next entry
//   -1 for couldn't read an entry, due to EOF or other fread error
//
int tf_compare_filename(FileSystem* fs, TFFile *fp, const char *name)
{
	u32 i;
	FatFileEntry entry;
	const char* compare_name=name;
	u32 lfn_entries;
	
	// Read a single directory entry
	tf_fread(fs, (u8*)&entry, sizeof(FatFileEntry), fp);
	
	// Fail if its bogus
	if(entry.msdos.filename[0] == 0x00) return -1;

	if (entry.msdos.attributes != 0x0f)
		SwizzleFF83(&entry.msdos);
	else
		SwizzleFFLFN(&entry.lfn);

	// If it's a DOS entry, then:
	if(entry.msdos.attributes != 0x0f)
	{
		// If it's a match, seek back an entry to the beginning of it, return 1
		if(1==tf_compare_filename_segment(&entry, name))
		{
			tf_fseek(fs, fp, -sizeof(FatFileEntry), fp->pos);
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else if ((entry.lfn.sequence_number & 0xc0) == 0x40)
	{
		//CHECK FOR 0x40 bit set or this is not the first (last) LFN entry!
		// If this is the first LFN entry, mask off the extra bit (0x40) and you get the number of entries in the chain
		lfn_entries = entry.lfn.sequence_number & ~0x40;
		
		// Seek to the last entry in the chain (LFN entries store name in reverse, so the last shall be first)
		tf_fseek(fs, fp, (s32)sizeof(FatFileEntry)*(lfn_entries-1), fp->pos);

		for(i=0; i<lfn_entries; i++)
		{
			// Seek back one and read it
			tf_fseek(fs, fp, -sizeof(FatFileEntry), fp->pos);
			tf_fread(fs, (u8*)&entry, sizeof(FatFileEntry), fp);
			SwizzleFFLFN(&entry.lfn);
		
			// Compare it.  If it's not a match, jump to the end of the chain, return failure 
			// Otherwise, continue looping until there's no entries left.
			if(!tf_compare_filename_segment(&entry, compare_name))
			{
				tf_fseek(fs, fp, (s32)((i))*sizeof(FatFileEntry), fp->pos);
				return 0;
			}

			tf_fseek(fs, fp, -sizeof(FatFileEntry), fp->pos);
			compare_name+=13;
		}

		// If we made it here, match was a success!  Return so... 
		// ONLY if next entry is valid!
		tf_fseek(fs, fp, (s32)sizeof(FatFileEntry)*lfn_entries, fp->pos);

		return 1;
	}

	return 0;
}

u32 tf_fread(FileSystem* fs, u8 *dest, u32 size, TFFile *fp)
{
	u32 sector;
	u32 requestedSize = size;
	while(size > 0)
	{
		// Fetch currect sector
		sector = tf_first_sector(fs, fp->currentCluster) + (fp->currentByte / 512);
		tf_fetch(fs, sector);

		u32 bytesLeftInSector = (512 - fp->currentByte % 512);
		if (bytesLeftInSector > size)
			bytesLeftInSector = size;

		if (((u32)dest & 1) == 0 && (fp->currentByte & 1) == 0)
		{
			//Aligned, copy words
			u32 wordsLeftInSector = bytesLeftInSector >> 1;
			u16 currentWord = (fp->currentByte % 512) >> 1;

			u16* buffer16 = (u16*)fs->tf_info.buffer;
			u16* dest16 = (u16*)dest;
			for (u32 i = 0; i < wordsLeftInSector; ++i)
				dest16[i] = buffer16[currentWord + i];

			//Copy last byte
			if (bytesLeftInSector & 1)
				dest[bytesLeftInSector - 1] = fs->tf_info.buffer[(fp->currentByte + bytesLeftInSector - 1) % 512];
		}
		else
		{
			//Copy bytes
			for (u32 i = 0; i < bytesLeftInSector; ++i)
				dest[i] = fs->tf_info.buffer[(fp->currentByte + i) % 512];
		}

		dest += bytesLeftInSector;
		size -= bytesLeftInSector;

		if (tf_fseek(fs, fp, 0, fp->pos + bytesLeftInSector))
			break;
	}

	return requestedSize - size;
}

u32 tf_fwrite(FileSystem* fs, const u8 *src, u32 size, TFFile *fp)
{
	u32 requestedSize = size;
	int tracking, segsize;

	//printHex(src, size);
	fp->flags |= TF_FLAG_DIRTY;
	fp->flags |= TF_FLAG_SIZECHANGED; //SKTD: Not true
	while(size > 0)
	{ 
		tf_fetch(fs, tf_first_sector(fs, fp->currentCluster) + (fp->currentByte / 512));
		tracking = fp->currentByte % 512;
		segsize = (size < 512 ? size : 512);
			
			
		memcpy( &fs->tf_info.buffer[ tracking ], src, segsize);
		fs->tf_info.sectorFlags |= TF_FLAG_DIRTY; // Mark this sector as dirty
			
		if (fp->pos + segsize > fp->size)
		{
			fp->size += segsize - (fp->pos % 512);
		}
			
	
		if(tf_unsafe_fseek(fs, fp, 0, fp->pos + segsize))
			break;
		
		size -= segsize;
		src += segsize;
	}

	return requestedSize - size;
}

int tf_fputs(FileSystem* fs, const char *src, TFFile *fp)
{
	return tf_fwrite(fs, (const u8*)src, strlen(src), fp);
}

int tf_fclose(FileSystem* fs, TFFile *fp)
{
	int rc;
	
	rc =  tf_fflush(fs, fp);
	fp->flags &= ~TF_FLAG_OPEN; // Mark the file as available for the system to use
	return rc;
}

/* tf_parent attempts to open the parent directory of whatever file you request

returns basically a fp the tf_fnopen returns
*/
TFFile *tf_parent(FileSystem* fs, const char *filename, const char *mode, int mkParents)
{
	TFFile *retval;
	const char *f2;

	f2 = strrchr(filename, '/');

	retval = tf_fnopen(fs, filename, "rw", (int)(f2-filename)+1);
	// if retval == NULL, why!?  we could be out of handles
	if (retval==NULL && mkParents)
	{
		// warning: recursion could fry some resources on smaller procs
		char tmpbuf[260];
		if (f2-filename>260)
			return NULL;
		
		strncpy(tmpbuf, filename, f2-filename);
		tmpbuf[f2-filename] = 0;

		tf_mkdir(fs, tmpbuf, mkParents );
		retval = tf_parent(fs, filename, mode, mkParents );
	}
	
	return retval;
}

int tf_fflush(FileSystem* fs, TFFile *fp)
{
	int rc = 0;
	TFFile *dir;
	FatFileEntry entry;
	const char *filename=entry.msdos.filename;

	if(!(fp->flags & TF_FLAG_DIRTY))
		return 0;

	// First write any pending data to disk
	if(fs->tf_info.sectorFlags & TF_FLAG_DIRTY)
		rc = tf_store(fs);

	// Now go modify the directory entry for this file to reflect changes in the file's size
	// (If they occurred)
	if(fp->flags & TF_FLAG_SIZECHANGED)
	{
		if(fp->attributes & 0x10)
		{
			// TODO Deal with changes in the root directory size here
		}
		else
		{
			// Open the parent directory
			dir = tf_parent(fs, fp->filename, "r+", false);
			if (dir == NULL)
				return -1;

			filename = strrchr(fp->filename, '/');
			
			// Seek to the entry we want to modify and pull it from disk
			if (tf_find_file(fs, dir, filename+1) == 0)
			{
				tf_fread(fs, (u8*)&entry, sizeof(FatFileEntry), dir);
				tf_fseek(fs, dir, -sizeof(FatFileEntry), dir->pos);

				// Modify the entry in place to reflect the new file size
				SwizzleFF83(&entry.msdos);
				entry.msdos.fileSize = fp->size - 1;
				SwizzleFF83(&entry.msdos);
				tf_fwrite(fs, (u8*)&entry, sizeof(FatFileEntry), dir); // Write fatfile entry back to disk
			}
			tf_fclose(fs, dir);
		}
		fp->flags &= ~TF_FLAG_SIZECHANGED;
	}
	
	fp->flags &= ~TF_FLAG_DIRTY;
	return rc;
}

/*
 * Remove a file from the filesystem
 * @param filename - The full path of the file to be removed
 * @return 
 */
int tf_remove(FileSystem* fs, const char *filename)
{
	TFFile *fp;
	FatFileEntry entry;
	int rc;
	u32 startCluster;

	// Sanity check
	fp = tf_fopen(fs, filename, "r");
	if(fp == NULL) return -1; // return an error if we're removing a file that doesn't exist
	startCluster = fp->startCluster; // Remember first cluster of the file so we can remove the clusterchain
	tf_fclose(fs, fp);

	// TODO Don't leave an orphaned LFN
	fp = tf_parent(fs, filename, "r+", false);
	rc = tf_find_file(fs, fp, (strrchr(filename, '/')+1));
	if(!rc)
	{
		while(1)
		{
			rc = tf_fseek(fs, fp, sizeof(FatFileEntry), fp->pos);
			if(rc) break;
			tf_fread(fs, (u8*)&entry, sizeof(FatFileEntry), fp); // Read one entry ahead
			tf_fseek(fs, fp, -((s32)2*sizeof(FatFileEntry)), fp->pos);
			tf_fwrite(fs, (u8*)&entry, sizeof(FatFileEntry), fp);
			if(entry.msdos.filename[0] == 0) break;
		}
		fp->size-=sizeof(FatFileEntry);
		fp->flags |= TF_FLAG_SIZECHANGED; 
	}

	tf_fclose(fs, fp);
	tf_free_clusterchain(fs, startCluster); // Free the data associated with the file

	return 0;

}

// Walk the FAT from the very first data sector and find a cluster that's available
// Return the cluster index 
// TODO: Rewrite this function so that you can start finding a free cluster at somewhere other than the beginning
u32 tf_find_free_cluster(FileSystem* fs)
{
	u32 i, entry, totalClusters;
	totalClusters = fs->tf_info.totalSectors/fs->tf_info.sectorsPerCluster;
	for(i=0;i<totalClusters; i++)
	{
		entry = tf_get_fat_entry(fs, i);
		if((entry & 0x0fffffff) == 0) break;
	}

	return i;
}

/* Optimize search for a free cluster */
u32 tf_find_free_cluster_from(FileSystem* fs, u32 c)
{
	u32 i, entry, totalClusters;

	totalClusters = fs->tf_info.totalSectors/fs->tf_info.sectorsPerCluster;
	for(i=c;i<totalClusters; i++)
	{
		entry = tf_get_fat_entry(fs, i);
		if((entry & 0x0fffffff) == 0) break;
	}

	/* We couldn't find anything here so search from the beginning */
	if (i == totalClusters)
		return tf_find_free_cluster(fs);

	return i;
}

/*! tf_get_open_handles()
	returns a bitfield where the handles are open (1) or free (0)
	assumes there are <32 handles
*/
u32 tf_get_open_handles(FileSystem* fs)
{
	int i;
	TFFile *fp;
	u32 retval = 0;

	for (i=0; i<min(TF_FILE_HANDLES, 32); i++)
	{
		retval <<= 1;
		fp = &fs->tf_file_handles[i];
		if(fp->flags & TF_FLAG_OPEN)
			retval |= 1;
	}
	
	return retval;
}

void Swizzle32(u32* val)
{
	u8* bytes = (u8*)val;
	u8 b = bytes[0];
	bytes[0] = bytes[3];
	bytes[3] = b;

	b = bytes[1];
	bytes[1] = bytes[2];
	bytes[2] = b;
}

void Swizzle16(u16* val)
{
	u8* bytes = (u8*)val;
	u8 b = bytes[0];
	bytes[0] = bytes[1];
	bytes[1] = b;
}

void SwizzleBPB32(BPB32_struct* bpb32)
{
	Swizzle32(&bpb32->FATSize);
	Swizzle16(&bpb32->ExtFlags);
	Swizzle16(&bpb32->FSVersion);
	Swizzle32(&bpb32->RootCluster);
	Swizzle16(&bpb32->FSInfo);
	Swizzle16(&bpb32->BkBootSec);
	Swizzle32(&bpb32->BS_VolumeID);
}

void SwizzleBPB(BPB_struct* bpb)
{
	Swizzle16(&bpb->BytesPerSector);
	Swizzle16(&bpb->ReservedSectorCount);
	Swizzle16(&bpb->RootEntryCount);
	Swizzle16(&bpb->TotalSectors16);
	Swizzle16(&bpb->FATSize16);
	Swizzle16(&bpb->SectorsPerTrack);
	Swizzle16(&bpb->NumberOfHeads);
	Swizzle32(&bpb->HiddenSectors);
	Swizzle32(&bpb->TotalSectors32);
	SwizzleBPB32(&bpb->FSTypeSpecificData.fat32);
}

void SwizzleFF83(FatFile83* ff)
{
	Swizzle16(&ff->creationTime);
	Swizzle16(&ff->creationDate);
	Swizzle16(&ff->lastAccessTime);
	Swizzle16(&ff->eaIndex);
	Swizzle16(&ff->modifiedTime);
	Swizzle16(&ff->modifiedDate);
	Swizzle16(&ff->firstCluster);
	Swizzle32(&ff->fileSize);
}

void SwizzleFFLFN(FatFileLFN* ff)
{
	//Nothing to do
	Swizzle16(&ff->name1[0]);
	Swizzle16(&ff->name1[1]);
	Swizzle16(&ff->name1[2]);
	Swizzle16(&ff->name1[3]);
	Swizzle16(&ff->name1[4]);
	
	Swizzle16(&ff->name2[0]);
	Swizzle16(&ff->name2[1]);
	Swizzle16(&ff->name2[2]);
	Swizzle16(&ff->name2[3]);
	Swizzle16(&ff->name2[4]);
	Swizzle16(&ff->name2[5]);

	Swizzle16(&ff->firstCluster);
	
	Swizzle16(&ff->name3[0]);
	Swizzle16(&ff->name3[1]);
}
