#include "files.h"
#include "string.h"

FileInfo **dirList, **dirPtr;
s16 dirListLen;
static u16 *mcPtr;

void initFiles(u16 *freeMem)
{
	dirList = (FileInfo **)freeMem;
	dirPtr = dirList;
	dirListLen = 0;
	mcPtr = freeMem + 16384*sizeof(void*);
}

void storeFile(const FileInfo* fileInfo)
{
	if (fileInfo->Attributes & (TF_ATTR_SYSTEM | TF_ATTR_VOLUME_LABEL))
		return; //Ignore these

	if ((fileInfo->Attributes & TF_ATTR_DIRECTORY) == 0)
	{
		FileInfo* entry = (FileInfo*)mcPtr;
		u16 fnLen = strlen(fileInfo->Filename);
		const char *ext = strrchr(fileInfo->Filename, '.');

		u16* outFilename16 = (u16*)entry->Filename;
		const u16* inFilename16 = (const u16*)fileInfo->Filename;

		u8 Attributes = fileInfo->Attributes & 0xF8;
		if (ext)
		{
			if (strcasecmp(ext, ".bin") == 0 || strcasecmp(ext, ".md") == 0)
			{
				//Assumed megadrive game
				fnLen = ext - fileInfo->Filename;
				Attributes |= MDROM;
			}
			else if (strcasecmp(ext, ".sms") == 0)
			{
				//Assumed master system game
				fnLen = ext - fileInfo->Filename;
				Attributes |= SMSROM;
			}
			else if (strcasecmp(ext, ".32x") == 0)
			{
				//Assumed 32x system game
				fnLen = ext - fileInfo->Filename;
				Attributes |= XROM;
			}
			else if (strcasecmp(ext, ".srm") == 0)
			{
				//Save file
				Attributes |= SAVE;
			}
		}

		u16 fnLen16 = (fnLen + 1) << 1;
		for (u16 i = 0; i < fnLen16; ++i)
			outFilename16[i] = inFilename16[i];
		entry->Filename[fnLen] = 0;

		entry->ParentStartCluster = fileInfo->ParentStartCluster;
		entry->StartCluster = fileInfo->StartCluster;
		entry->Attributes = Attributes;
		entry->Size = fileInfo->Size;

		mcPtr = outFilename16 + fnLen16 + 1;
		*dirPtr++ = entry;
	}
	else if (fileInfo->Size > 0)//Directory
	{
		FileInfo* entry = (FileInfo*)mcPtr;
		u16 fnLen = strlen(fileInfo->Filename);
		u16* outFilename16 = (u16*)entry->Filename;
		const u16* inFilename16 = (const u16*)fileInfo->Filename;
		u16 fnLen16 = (fnLen + 1) << 1;

		if (strcmp(fileInfo->Filename, ".") == 0)
			return; //Ignore this directory

		//Gotta limit the name to something
		if (fnLen > 64)
			fnLen = 64;

		for (u16 i = 0; i < fnLen16; ++i)
			outFilename16[i] = inFilename16[i];
		entry->Filename[fnLen] = 0;

		entry->ParentStartCluster = fileInfo->ParentStartCluster;
		entry->StartCluster = fileInfo->StartCluster;
		entry->Attributes = fileInfo->Attributes;
		entry->Size = fileInfo->Size;

		mcPtr = outFilename16 + fnLen16 + 1;
		*dirPtr++ = entry;
	}

	dirListLen = dirPtr - dirList;
}

static inline char toUpper(char c)
{
	return ('a' <= c && c <= 'z') ? c ^ 0x20 : c;
}

static inline short myStrCmp(const char *x, const char *y)
{
	short retVal;
	while ( *x && *y && toUpper(*x) == toUpper(*y) ) {
		x++;
		y++;
	}
	retVal = toUpper(*x);
	retVal -= toUpper(*y);
	return retVal;
}

short myFileComp(CVPtr x, CVPtr y)
{
	const FileInfo *u = (const FileInfo *)x;
	const FileInfo *v = (const FileInfo *)y;

	if ((u->Attributes ^ v->Attributes) & TF_ATTR_DIRECTORY)
	{
		//One is a directory and the other isn't
		if (u->Attributes & TF_ATTR_DIRECTORY)
			return -1;
		else
			return 1;
	}

	return myStrCmp(u->Filename, v->Filename);
}
