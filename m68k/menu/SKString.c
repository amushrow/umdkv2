#include "genesis.h"
#include "SKString.h"

bool CheckFlags(int value, int flags)
{
	return (value & flags) == flags;
}

CodePoint GetNextCodePoint(const char* str, u16* pos)
{
	u8 ByteMask[] = { 192, 224, 240 };
	u8* data = (u8*)str;
	u16 startPosition = *pos;
	CodePoint CP = 0;

	if (CheckFlags(data[startPosition], ByteMask[2]))
	{
		CP = ((u32)data[startPosition] & 0x07) << 18;
		CP |= ((u32)data[startPosition + 1] & 0x3F) << 12;
		CP |= ((u32)data[startPosition + 2] & 0x3F) << 6;
		CP |= (u32)data[startPosition + 3] & 0x3F;

		*pos = startPosition + 4;
	}
	else if (CheckFlags(data[startPosition], ByteMask[1]))
	{
		CP = ((u32)data[startPosition] & 0x0F) << 12;
		CP |= ((u32)data[startPosition + 1] & 0x3F) << 6;
		CP |= (u32)data[startPosition + 2] & 0x3F;

		*pos = startPosition + 3;
	}
	else if (CheckFlags(data[startPosition], ByteMask[0]))
	{
		CP = ((u32)data[startPosition] & 0x1F) << 6;
		CP |= (u32)data[startPosition + 1] & 0x3F;

		*pos = startPosition + 2;
	}
	else
	{
		CP = data[startPosition];
		if (CP != 0)
			*pos = startPosition + 1;
	}

	return CP;
}

u16 FindTileIndex(CodePoint cp, const CharMap* map)
{
	u16 start = 0;
	u16 end = map->Length - 1;
	u16 mid = (end - start) / 2;

	while (1)
	{
		if (map->CodePoints[mid] == cp)
		{
			return mid;
		}
		else if (mid == start)
		{
			return map->Length - 1; //CodePoint is not in map, tile 0 should be used for invalid chars
		}
		else if (map->CodePoints[mid] > cp)
		{
			end = mid;
			mid = start + ((end - start) / 2);
		}
		else if (map->CodePoints[mid] < cp)
		{
			start = mid;
			mid = start + ((end - start) / 2);
		}
	}
	
	return 0;
}

void DrawUnicodeString(VDPPlan plan, const char* str, const CharMap* map, u16 x, u16 y)
{
	u16 data[128];

	//Max length we can print (without spilling over onto a new line)
	u16 len = VDP_getPlanWidth() - x;
	u16 i = 0;
	u16 strPos = 0;
	CodePoint nextCodePoint = GetNextCodePoint(str, &strPos);
	
	while (1)
	{
		if (nextCodePoint != 0 && i < len)
		{
			data[i] = TILE_USERINDEX + FindTileIndex(nextCodePoint, map);
		}
		else
		{
			break;
		}

		++i;
		nextCodePoint = GetNextCodePoint(str, &strPos);
	}

	//Set length to length of string
	len = i;

	u16 baseTile = 0;
	baseTile |= (VDP_getTextPalette() & 3) << 13;
	baseTile |= VDP_getTextPriority() << 15;
	

	VDP_setTileMapDataRectEx(plan, data, baseTile, x, y, len, 1, len);
}