#ifndef SKSTRING_H
#define SKSTRING_H

#include "vdp.h"
#include "types.h"

typedef u16 CodePoint;

typedef struct _CharMap
{
	u16 Length;
	CodePoint CodePoints[1];
} CharMap;

CodePoint GetNextCodePoint(const char* str, u16* pos);
u16 FindTileIndex(CodePoint cp, const CharMap* map);
void DrawUnicodeString(VDPPlan plan, const char* str, const CharMap* map, u16 x, u16 y);

#endif