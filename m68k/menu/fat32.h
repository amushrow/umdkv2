#ifndef FAT32_H
#define FAT32_H

#include "thinfat32/thinfat32.h"
#include "thinfat32/thinternal.h"
#include "sdcard.h"

//-- Interface for fat32 implementation, so it's easier to swap out in future
typedef TFFile* FileHandle;
#define NullFileHandle NULL

typedef struct _struct_FileInfo
{
	u32   ParentStartCluster;
	u32   StartCluster;
	u32   Size;
	u8    Attributes;
	u8    Pad;
	char  Filename[1];
} FileInfo;

typedef void (*FileInfoCallback)(const FileInfo *);

// Get the required size for the file system to run
u32        fatGetMemoryFootprint();

// Init the file system
int        fatOpenFileSystem(FileSystem* fs);

// List the contents of a directory
void       fatListDirectory(FileSystem* fs, const char* directoryName, FileInfoCallback callback);

// List the contents of a directory
void       fatListDirectoryFI(FileSystem* fs, const FileInfo* directoryInfo, FileInfoCallback callback);

// Open a file
FileHandle fatOpenFile(FileSystem* fs, const char* filePath, const char* mode);

// Open a file
FileHandle fatOpenFileFI(FileSystem* fs, const FileInfo* fileInfo, const char* mode);

// Close an open file
void       fatCloseFile(FileSystem* fs, FileHandle file);

// Read data from an open file. Return the number of bytes read
u32        fatReadFile(FileSystem* fs, FileHandle file, u8* outBuffer, u32 length);

// Write data to an open file. Return the number of bytes written
u32        fatWriteFile(FileSystem* fs, FileHandle file, const u8* inBuffer, u32 length);

// Set read/write position in the file. Returns the new position
u32        fatSetFilePos(FileSystem* fs, FileHandle file, u32 pos);

// Returns the current read/write position in the file
u32        fatGetFilePos(FileSystem* fs, FileHandle file);

// Gets the size of the open file
u32        fatGetFileSize(FileSystem* fs, FileHandle file);

// Creates all directories and subdirectories in the path
u8         fatCreateDirectory(FileSystem* fs, const char* directoryPath);

// Creates all directories and subdirectories in the path, relative to the directory pointed by directoryInfo
u8         fatCreateDirectoryFI(FileSystem* fs, FileInfo* directoryInfo, const char* directoryPath);

// Creates a file
u8         fatCreateFile(FileSystem* fs, const char* filePath);

// Creates a file, relative to the directory pointed by directoryInfo
u8         fatCreateFileFI(FileSystem* fs, FileInfo* directoryInfo, const char* fileName);

// Deletes a file / directory.
u8         fatDelete(FileSystem* fs, const char* path, bool recursive);

// Deletes a file / directory.
u8         fatDeleteFI(FileSystem* fs, FileInfo* info, bool recursive);

// Gets the cluster length of the open file system
u32        fatGetClusterLength(FileSystem* fs);

// Direct read a cluster
u32        fatReadCluster(FileSystem* fs, u32 cluster, u8 *buffer);
#endif
