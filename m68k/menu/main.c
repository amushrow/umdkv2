﻿#include "genesis.h"
#include "fat32.h"
#include "qsort.h"
#include "files.h"
#include "sdcard.h"
#include "menu.h"
#include "res/menures.h"
#include "SKString.h"

extern const CharMap char_map;

//-- Memory Map
// 0xB00000:0xB00400 - Monitor
// 0xB00400:0xB10500 - GDB Communication
// 0xB10500:0xB10D00 - File system memory
// 0xB10D00:0xB10D02 - Save Flag / Timer (modified to by the fpga firmware when a game save is written)
// 0xB10D02:0xB10E00 - VInt data
// 0xB10E00:0xB20000 - Stack memory (so we don't use the megadrive ram during our VInt shenanigans)
// 0xB20000:0xB30000 - Menu (VINTBootstrap: 0xB20200, Program Start: 0xB20220)
// 0xB30000:0xB70000 - Unused
// 0xB70000:0xB80000 - SRAM
//--

typedef struct _struct_LaunchOptions
{
	u16 ForceRegion;
	u16 EnableSaves;
	u16 StartNewSave;
	
	u16 MenuItemIsDirectory;
	u16 SelectedOption;
	u16 AutoRepeatDelay;
} LaunchOptions;

#define FILESYSTEM_PTR 0xB10500
#define SAVETIMER_PTR 0xB10D00
#define VINTDATA_PTR 0xB10D02
#define STACK_PTR 0xB1FFFE
#define SRAM_PTR 0xB70000

//ROM Header Offsets
#define ROM_SAVESTART 0x1B4
#define ROM_SAVEEND 0x1B8
#define ROM_INTERNATIONAL_NAME 0x150
#define ROM_REGION 0x1F0

#define FORCE_REGION_NONE    0x00
#define FORCE_REGION_JAPAN   0x04
#define FORCE_REGION_AMERICA 0x06
#define FORCE_REGION_EUROPE  0x07

static u16 _BackBuffer;
static u16 _FrontBuffer;
static s16 _ActiveMenuScreen;
static MenuData _Menu;
static FileSystem* _FS;
static LaunchOptions _LaunchOptions;

typedef struct _VINTData
{
	u32 StackPointer; //Must be first
	char SaveFileName[1];
} VINTData;



void VINTBootstrap(void);
void VINT_Injected()
{
	//Don't access any global variables like _NumFiles, or _FS in here as they will be set to the MD's ram
	//which has long since been replaced with the running game
	asm("moveml %d0-%a6,-(%sp)");   //Store everything

	u16* saveTimer = (u16*)SAVETIMER_PTR;
	if (*saveTimer > 0)
	{
		--(*saveTimer);
		if (*saveTimer == 0)
		{
			//Move save data to SD card
			FileSystem* fs = (FileSystem*)FILESYSTEM_PTR;
			u8* SaveBuff = (u8*)SRAM_PTR;

			sdInit();
			fatOpenFileSystem(fs);

			//Get save size from header
			u32 SaveStart = *(u32*)ROM_SAVESTART;
			u32 SaveEnd = *(u32*)ROM_SAVEEND;
			u32 SaveSize = ((SaveEnd - SaveStart) + 511) / 512; //Round up to nearest 512 block
			SaveSize *= 512;

			//Save out the file
			VINTData* data = (VINTData*)VINTDATA_PTR;
			fatCreateDirectory(fs, "/saves");
			FileHandle SaveFile = fatOpenFile(fs, data->SaveFileName, "w");
			fatWriteFile(fs, SaveFile, SaveBuff, SaveSize);
			fatCloseFile(fs, SaveFile);
		}
	}

	asm("moveml (%sp)+,%d0-%a6");   //Put everything back
}

void flipBuffers()
{
	const s16 BufferHeight = 32;
	if (_BackBuffer == 0)
	{
		_BackBuffer = BufferHeight;
		_FrontBuffer = 0;
		VDP_setVerticalScroll(PLAN_A, 0);
		VDP_setVerticalScroll(PLAN_B, 0);
	}
	else
	{
		_BackBuffer = 0;
		_FrontBuffer = BufferHeight;
		VDP_setVerticalScroll(PLAN_A, BufferHeight * 8);
		VDP_setVerticalScroll(PLAN_B, BufferHeight * 8);
	}
}

void loadRom(s16 choice)
{
	const FileInfo *const file = dirList[choice];

	const u32 clusterLen = fatGetClusterLength(_FS);
	const u32 numClustersToRead = (file->Size + clusterLen - 1) / clusterLen;
	const u32 bytesPerStar = (numClustersToRead * clusterLen) / 32;

	const char *str = file->Filename;
	u32 cluster = file->StartCluster;
	u16 x = 0, i;
	volatile u8 *const ssf2Reg = (volatile u8 *)0xA130FB;
	u8 page = 0x00;
	u8 *const LWM = (u8*)0x280000;
	u8 *const HWM = LWM + 512 * 1024;
	u8 *ptr = LWM;
	u8 numStars = 0;

	//UMDK control register bits [3: Enable Saves, 2: Enable Region Hack, 1-0: Region]
	u16 * const VersionOverrideReg = (u16*)0xA130E0;
	*VersionOverrideReg = (_LaunchOptions.EnableSaves << 3) | _LaunchOptions.ForceRegion;

	// Work out what x offset to use for the ROM name
	while (*str)
	{
		str++;
		x++;
	}
	x = (40 - x) >> 1;
	str = dirList[choice]->Filename;

	VINTData* data = (VINTData*)VINTDATA_PTR;
	strcpy(data->SaveFileName, "/saves/");
	strcat(data->SaveFileName, dirList[choice]->Filename);

	char* ext = (char*)strrchr(data->SaveFileName, '.');
	if (ext)
		strcpy(ext, ".srm");
	else
		strcat(data->SaveFileName, ".srm");

	VDP_setTextPalette(PAL0);

	// Construct loading screen
	VDP_waitVSync();
	VDP_clearPlan(PLAN_A, 1);
	VDP_clearPlan(PLAN_B, 1);
	VDP_waitDMACompletion();
	VDP_setTextPriority(1);
	DrawUnicodeString(PLAN_A, str, &char_map, x, _FrontBuffer + 11);
	DrawUnicodeString(PLAN_A, "Loading...", &char_map, 15, _FrontBuffer + 24);
	DrawUnicodeString(PLAN_A, "╔════════════════════════════════╗", &char_map, 3, _FrontBuffer + 25);
	DrawUnicodeString(PLAN_A, "║                                ║", &char_map, 3, _FrontBuffer + 26);
	DrawUnicodeString(PLAN_A, "╚════════════════════════════════╝", &char_map, 3, _FrontBuffer + 27);
	*ssf2Reg = page;

	// Write cluster length
	*((u16*)0xB30000) = (clusterLen >> 2) - 1;
	for (i = 0; i < numClustersToRead; ++i)
	{
		if (i == 0)
			cluster = fatReadCluster(_FS, cluster, (u8*)0xB30002);
		else
			cluster = fatReadCluster(_FS, cluster, ptr);

		ptr += clusterLen;
		if (ptr == HWM)
		{
			ptr = LWM;
			page++;
			*ssf2Reg = page;
		}

		x = ((i + 1) * clusterLen) / bytesPerStar;
		for (; numStars < x; ++numStars)
			VDP_drawText("*", numStars + 4, _FrontBuffer + 26);
	}

	//Inject our own code into VINT
	if (_LaunchOptions.EnableSaves)
	{
		u32 OldVint = *(u32*)0xB3007A;     //Get address of the original VINT
		u32 NewVint = (u32)&VINTBootstrap; //Get the address of our VINT (should be at 0xB20200)
		*(u32*)(NewVint + 26) = OldVint;   //Write old VINT in the address of VIntBootstrap's jmp instruction 
		*(u16*)SAVETIMER_PTR = 0;          //Clear the save timer

		//Load the save file into memory too
		u8* SaveBuff = (u8*)0xB70000;
		FileHandle saveFile = fatOpenFile(_FS, data->SaveFileName, "r");
		if (!_LaunchOptions.StartNewSave && saveFile != NullFileHandle)
		{
			fatReadFile(_FS, saveFile, SaveBuff, fatGetFileSize(_FS, saveFile));
			fatCloseFile(_FS, saveFile);
		}
		else
		{
			u16* SaveBuff16 = (u16*)0xB70000;
			for (i = 0; i < 800; ++i)
				SaveBuff16[i] = 0;
		}
	}

	*ssf2Reg = 0x00; // point back at bottom of RAM
	__asm__("trap #0");
}

void doSelect(s16 choice)
{
	const FileInfo *const file = dirList[choice];
	if (file->Attributes & TF_ATTR_DIRECTORY)
	{
		FileInfo Copy = *file;

		//Reset files list, and load up a new directory
		initFiles((u16*)0xB40000);
		fatListDirectoryFI(_FS, &Copy, storeFile);

		quickSort((CVPtr *)dirList, 0, dirListLen, (CompareFunc)myFileComp);
		menuInit(&_Menu);

		return;
	}
	else //If ROM
	{
		loadRom(choice);
	}
}

void drawHelp(u16 buffer)
{
	VDP_setTextPriority(0);
	DrawUnicodeString(PLAN_B, "                            ", &char_map, 6, buffer + 11);
	DrawUnicodeString(PLAN_B, "                            ", &char_map, 6, buffer + 12);
	DrawUnicodeString(PLAN_B, "                            ", &char_map, 6, buffer + 13);
	DrawUnicodeString(PLAN_B, "                            ", &char_map, 6, buffer + 14);
	DrawUnicodeString(PLAN_B, "                            ", &char_map, 6, buffer + 15);

	VDP_setTextPriority(1);
	DrawUnicodeString(PLAN_A, "╔══════════════════════════╗", &char_map, 6, buffer + 10);
	DrawUnicodeString(PLAN_A, "║        A: Show Help      ║", &char_map, 6, buffer + 11);
	DrawUnicodeString(PLAN_A, "║  B(Hold): Fast Scroll    ║", &char_map, 6, buffer + 12);
	DrawUnicodeString(PLAN_A, "║        C: Confirm        ║", &char_map, 6, buffer + 13);
	DrawUnicodeString(PLAN_A, "║    Start: Launch Options ║", &char_map, 6, buffer + 14);
	DrawUnicodeString(PLAN_A, "╚══════════════════════════╝", &char_map, 6, buffer + 15);
}

void drawLaunchOptions(u16 buffer)
{
	if (_LaunchOptions.SelectedOption == 0)
	{
		VDP_setTextPriority(1);
		VDP_setTextPalette(PAL1);
		DrawUnicodeString(PLAN_A, "--LAUNCH--", &char_map, 15, buffer + 11);
		VDP_setTextPalette(PAL0);
	}
	else
	{
		VDP_setTextPriority(0);
		DrawUnicodeString(PLAN_A, "--LAUNCH--", &char_map, 15, buffer + 11);
	}

	VDP_setTextPalette(PAL0);
	VDP_setTextPriority(_LaunchOptions.SelectedOption == 1);
	if (_LaunchOptions.ForceRegion == FORCE_REGION_JAPAN)
		DrawUnicodeString(PLAN_A, "Japan   ", &char_map, 24, buffer + 12);
	else if (_LaunchOptions.ForceRegion == FORCE_REGION_AMERICA)
		DrawUnicodeString(PLAN_A, "USA     ", &char_map, 24, buffer + 12);
	else if (_LaunchOptions.ForceRegion == FORCE_REGION_EUROPE)
		DrawUnicodeString(PLAN_A, "Europe  ", &char_map, 24, buffer + 12);
	else
		DrawUnicodeString(PLAN_A, "Disabled", &char_map, 24, buffer + 12);

	VDP_setTextPriority(_LaunchOptions.SelectedOption == 2);
	if (_LaunchOptions.EnableSaves)
		DrawUnicodeString(PLAN_A, "Yes", &char_map, 24, buffer + 13);
	else
		DrawUnicodeString(PLAN_A, "No ", &char_map, 24, buffer + 13);

	VDP_setTextPriority(_LaunchOptions.SelectedOption == 3);
	if (_LaunchOptions.StartNewSave)
		DrawUnicodeString(PLAN_A, "Yes", &char_map, 24, buffer + 14);
	else
		DrawUnicodeString(PLAN_A, "No ", &char_map, 24, buffer + 14);
}

void handleLaunchOptionsInput(u16 oldJoy, u16 newJoy)
{
	u16 MinOption = _LaunchOptions.MenuItemIsDirectory ? 1 : 0;
	
	// Scroll up
	if (newJoy & BUTTON_UP)
	{
		if (!(oldJoy & BUTTON_UP) || _LaunchOptions.AutoRepeatDelay == 0)
		{
			if (_LaunchOptions.SelectedOption > MinOption)
				_LaunchOptions.SelectedOption--;
			else
				_LaunchOptions.SelectedOption = 3;

			if (!(oldJoy & BUTTON_UP))
				_LaunchOptions.AutoRepeatDelay = 15;
			else
				_LaunchOptions.AutoRepeatDelay = 4;
		}
	}

	// Scroll down
	if (newJoy & BUTTON_DOWN)
	{
		if (!(oldJoy & BUTTON_DOWN) || _LaunchOptions.AutoRepeatDelay == 0)
		{
			if (_LaunchOptions.SelectedOption < 3)
				_LaunchOptions.SelectedOption++;
			else
				_LaunchOptions.SelectedOption = MinOption;

			if (!(oldJoy & BUTTON_DOWN))
				_LaunchOptions.AutoRepeatDelay = 15;
			else
				_LaunchOptions.AutoRepeatDelay = 4;
		}
	}

	u16 Pressed_C = (newJoy & BUTTON_C) && !(oldJoy & BUTTON_C);

	if (newJoy & BUTTON_LEFT)
	{
		if (!(oldJoy & BUTTON_LEFT) || _LaunchOptions.AutoRepeatDelay == 0)
		{
			if (!(oldJoy & BUTTON_LEFT))
				_LaunchOptions.AutoRepeatDelay = 15;
			else
				_LaunchOptions.AutoRepeatDelay = 4;

			if (_LaunchOptions.SelectedOption == 1) //Region
			{
				switch (_LaunchOptions.ForceRegion)
				{
				case FORCE_REGION_NONE: _LaunchOptions.ForceRegion = FORCE_REGION_EUROPE;  break;
				case FORCE_REGION_JAPAN: _LaunchOptions.ForceRegion = FORCE_REGION_NONE; break;
				case FORCE_REGION_AMERICA: _LaunchOptions.ForceRegion = FORCE_REGION_JAPAN; break;
				case FORCE_REGION_EUROPE: _LaunchOptions.ForceRegion = FORCE_REGION_AMERICA; break;
				}
			}
			else if (_LaunchOptions.SelectedOption == 2) //Enable saves
			{
				_LaunchOptions.EnableSaves = 1 - _LaunchOptions.EnableSaves;
			}
			else if (_LaunchOptions.SelectedOption == 3) //Start new save
			{
				_LaunchOptions.StartNewSave = 1 - _LaunchOptions.StartNewSave;
			}
		}
	}
	else if ((newJoy & BUTTON_RIGHT) || Pressed_C)
	{
		if (!(oldJoy & BUTTON_RIGHT) || _LaunchOptions.AutoRepeatDelay == 0 || Pressed_C)
		{
			if (!(oldJoy & BUTTON_RIGHT))
				_LaunchOptions.AutoRepeatDelay = 15;
			else
				_LaunchOptions.AutoRepeatDelay = 4;

			if (_LaunchOptions.SelectedOption == 1)  //Region
			{
				switch (_LaunchOptions.ForceRegion)
				{
				case FORCE_REGION_NONE: _LaunchOptions.ForceRegion = FORCE_REGION_JAPAN;  break;
				case FORCE_REGION_JAPAN: _LaunchOptions.ForceRegion = FORCE_REGION_AMERICA; break;
				case FORCE_REGION_AMERICA: _LaunchOptions.ForceRegion = FORCE_REGION_EUROPE; break;
				case FORCE_REGION_EUROPE: _LaunchOptions.ForceRegion = FORCE_REGION_NONE; break;
				}
			}
			else if (_LaunchOptions.SelectedOption == 2) //Enable saves
			{
				_LaunchOptions.EnableSaves = 1 - _LaunchOptions.EnableSaves;
			}
			else if (_LaunchOptions.SelectedOption == 3) //Start new save
			{
				_LaunchOptions.StartNewSave = 1 - _LaunchOptions.StartNewSave;
			}
		}
	}

	if (_LaunchOptions.SelectedOption == 0 && Pressed_C)
	{
		const FileInfo *const file = dirList[_Menu.choice];
		if ((file->Attributes & TF_ATTR_DIRECTORY) == 0)
		{
			loadRom(_Menu.choice);
		}
	}

	if ((newJoy & BUTTON_START) && !(oldJoy & BUTTON_START))
	{
		NextMenuScreen = MENU_MAIN_SCREEN;
	}
}

void enterMenuScreen(u16 menuScreen)
{
	if (menuScreen == MENU_MAIN_SCREEN)
	{
		//Re-draw everything
		menuDraw(&_Menu, _BackBuffer);
		menuUpdateScrollingText(&_Menu, _BackBuffer);
		VDP_waitVSync();
		flipBuffers();	
	}
	else if (menuScreen == MENU_HELP_SCREEN)
	{
		//Reset scrolling text so it looks nicer when we draw over it
		menuResetScrollingTextPosition(&_Menu);
		menuUpdateScrollingText(&_Menu, _FrontBuffer);
		drawHelp(_FrontBuffer);
	}
	else if (menuScreen == MENU_LAUNCH_OPTIONS_SCREEN)
	{
		const FileInfo *const file = dirList[_Menu.choice];
		if (file->Attributes & TF_ATTR_DIRECTORY)
		{
			_LaunchOptions.MenuItemIsDirectory = 1;
			_LaunchOptions.SelectedOption = 1;
		}
		else
		{
			_LaunchOptions.SelectedOption = 0;
			_LaunchOptions.MenuItemIsDirectory = 0;
		}
		
		//Draw the menu static elements
		VDP_setTextPriority(0);
		DrawUnicodeString(PLAN_B, "                            ", &char_map, 6, _FrontBuffer + 11);
		DrawUnicodeString(PLAN_B, "                            ", &char_map, 6, _FrontBuffer + 12);
		DrawUnicodeString(PLAN_B, "                            ", &char_map, 6, _FrontBuffer + 13);
		DrawUnicodeString(PLAN_B, "                            ", &char_map, 6, _FrontBuffer + 14);
		DrawUnicodeString(PLAN_B, "                            ", &char_map, 6, _FrontBuffer + 15);

		VDP_setTextPriority(1);
		DrawUnicodeString(PLAN_A, "╔══════════════════════════╗", &char_map, 6, _FrontBuffer + 10);
		DrawUnicodeString(PLAN_A, "║                          ║", &char_map, 6, _FrontBuffer + 11);
		DrawUnicodeString(PLAN_A, "║   Force Region:          ║", &char_map, 6, _FrontBuffer + 12);
		DrawUnicodeString(PLAN_A, "║   Enable Saves:          ║", &char_map, 6, _FrontBuffer + 13);
		DrawUnicodeString(PLAN_A, "║ Start New Save:          ║", &char_map, 6, _FrontBuffer + 14);
		DrawUnicodeString(PLAN_A, "╚══════════════════════════╝", &char_map, 6, _FrontBuffer + 15);

		drawLaunchOptions(_FrontBuffer);
	}
}

void sysVint()
{
	if (_ActiveMenuScreen == MENU_MAIN_SCREEN)
	{
		menuTick(&_Menu);
	}
	else if (_ActiveMenuScreen == MENU_LAUNCH_OPTIONS_SCREEN)
	{
		if (_LaunchOptions.AutoRepeatDelay > 0)
			--_LaunchOptions.AutoRepeatDelay;
	}
}

int main()
{
	u16 newJoy = 0, oldJoy = 0;	
	_ActiveMenuScreen = MENU_MAIN_SCREEN;
	NextMenuScreen = MENU_MAIN_SCREEN;
	_LaunchOptions.EnableSaves = 1;
	_LaunchOptions.StartNewSave = 0;
	_LaunchOptions.ForceRegion = 0;
	
	// Init the screen, display message
	VDP_setScreenWidth320();
	VDP_setHInterrupt(0);
	VDP_setTextPalette(PAL0);
	VDP_setHilightShadow(0);
	VDP_setTextPriority(1);

	VDP_setPlanSize(64, 64);
	VDP_setSpriteListAddress(0xA800);
	VDP_setHScrollTableAddress(0xAC00);
	VDP_setWindowAddress(0xB000);
	VDP_setBPlanAddress(0xC000);
	VDP_setAPlanAddress(0xE000);

	VDP_setScrollingMode(HSCROLL_PLANE, VSCROLL_PLANE);
	VDP_setVerticalScroll(PLAN_A, 0);
	VDP_setHorizontalScroll(PLAN_A, 0);
	VDP_setHorizontalScroll(PLAN_B, 0);

	_FrontBuffer = 0;
	_BackBuffer = 32;
	SYS_setVIntPreCallback(sysVint);

	VDP_loadTileSet(&extended_font, TILE_USERINDEX, DMA);

	//Text colours
	PAL_setPaletteColors(PAL0, &extended_font_pal);
	VDP_setPaletteColor(31, RGB24_TO_VDPCOLOR(0xFF0000));

	DrawUnicodeString(PLAN_A, "MakeStuff USB MegaDrive Dev Kit v2", &char_map, 3, 10);
	DrawUnicodeString(PLAN_A, "Initialising SD-card...", &char_map, 4, 12);

	// Initialise the SD card
	sdInit();

	DrawUnicodeString(PLAN_A, "Opening File System...", &char_map, 4, 13);
	
	// Get the geometry of the first SD-card partition
	_FS = (FileSystem*)FILESYSTEM_PTR;
	fatOpenFileSystem(_FS);

	// Initialise workspace for the directory list
	initFiles((u16*)0xB40000);

	DrawUnicodeString(PLAN_A, "Reading SD-card...", &char_map, 4, 14);

	fatListDirectory(_FS, "/", storeFile);

	// Sort the list alphabetically
	quickSort((CVPtr *)dirList, 0, dirListLen, (CompareFunc)myFileComp);

	// Draw menu bits
	VDP_clearPlan(PLAN_A, 1);
	VDP_waitDMACompletion();

	menuInit(&_Menu);
	menuDrawBorders(&_Menu, _BackBuffer);
	menuDrawBorders(&_Menu, _FrontBuffer);
	menuDraw(&_Menu, _FrontBuffer);
	menuUpdateScrollingText(&_Menu, _FrontBuffer);

	// Display the list
	for (;;)
	{
		newJoy = JOY_readJoypad(0);

		if (_ActiveMenuScreen != NextMenuScreen)
		{
			_ActiveMenuScreen = NextMenuScreen;
			enterMenuScreen(_ActiveMenuScreen);
		}

		if (_ActiveMenuScreen == MENU_MAIN_SCREEN)
		{
			menuInput(&_Menu, oldJoy, newJoy);

			if (menuOptionSelected(&_Menu))
			{
				doSelect(_Menu.choice);
			}

			if (menuOptionChanged(&_Menu))
			{
				menuDraw(&_Menu, _BackBuffer);
				VDP_waitVSync();
				flipBuffers();
			}
			else
			{
				VDP_waitVSync();
			}

			menuUpdateScrollingText(&_Menu, _FrontBuffer);
		}
		else if (_ActiveMenuScreen == MENU_HELP_SCREEN)
		{
			if ((newJoy & (BUTTON_A)) && !(oldJoy & (BUTTON_A)))
			{
				NextMenuScreen = MENU_MAIN_SCREEN;
			}

			VDP_waitVSync();
		}
		else if (_ActiveMenuScreen == MENU_LAUNCH_OPTIONS_SCREEN)
		{
			handleLaunchOptionsInput(oldJoy, newJoy);
			VDP_waitVSync();
			drawLaunchOptions(_FrontBuffer);
		}

		oldJoy = newJoy;
	}
}
